package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Axis int

const (
	X Axis = iota
	Y
)

type FoldInstruction struct {
	axis  Axis
	value int
}

type Origami struct {
	dots         []Point
	instructions []FoldInstruction
}

func (o *Origami) dotCount() int {
	return len(o.dots)
}

func (o *Origami) nextFold() Origami {
	fold := o.instructions[0]

	newOrigami := Origami{}
	newOrigami.instructions = o.instructions[1:]

	newDots := map[Point]bool{}
	if fold.axis == X {
		// folding right over to left
		for _, dot := range o.dots {
			if dot.x > fold.value {
				newDot := Point{x: fold.value - (dot.x - fold.value), y: dot.y}
				// fmt.Println(dot, "folds to", newDot)
				newDots[newDot] = true
				// newOrigami.dots = append(newOrigami.dots, newPoint)
			} else {
				newDots[dot] = true
				// newOrigami.dots = append(newOrigami.dots, Point{x: dot.x, y: dot.y})
			}
		}
	} else {
		// folding bottom up
		for _, dot := range o.dots {
			if dot.y > fold.value {
				newDot := Point{x: dot.x, y: fold.value - (dot.y - fold.value)}
				// fmt.Println(dot, "folds to", newDot)
				newDots[newDot] = true
				// newOrigami.dots = append(newOrigami.dots, newPoint)
			} else {
				newDots[dot] = true
				// newOrigami.dots = append(newOrigami.dots, Point{x: dot.x, y: dot.y})
			}
		}
	}

	for k := range newDots {
		newOrigami.dots = append(newOrigami.dots, k)
	}
	return newOrigami
}

func (o *Origami) dotAt(x, y int) bool {
	p := Point{x: x, y: y}
	for _, dot := range o.dots {
		if p == dot {
			return true
		}
	}
	return false
}

func (o *Origami) print() {
	maxX := 0
	maxY := 0
	for _, dot := range o.dots {
		if dot.x > maxX {
			maxX = dot.x
		}
		if dot.y > maxY {
			maxY = dot.y
		}
	}

	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			if o.dotAt(x, y) {
				fmt.Printf("#")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Println("")
	}
}

func loadOrigami() Origami {
	file, _ := os.Open("day13.txt")
	defer file.Close()

	origami := Origami{}

	scanner := bufio.NewScanner(file)
	instructionSection := false
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			instructionSection = true
		} else if instructionSection {
			coords := line[11:]
			parts := strings.Split(coords, "=")
			axis := X
			if parts[0] == "y" {
				axis = Y
			}
			value, _ := strconv.Atoi(parts[1])
			origami.instructions = append(origami.instructions, FoldInstruction{axis: axis, value: value})
		} else {
			parts := strings.Split(line, ",")
			x, _ := strconv.Atoi(parts[0])
			y, _ := strconv.Atoi(parts[1])
			origami.dots = append(origami.dots, Point{x: x, y: y})
		}
	}

	return origami
}

func Day13_1() {
	origami := loadOrigami()
	newOrigami := origami.nextFold()
	fmt.Println("day 13-1:", newOrigami.dotCount(), "(number of dots post first fold)")
}

func Day13_2() {
	origami := loadOrigami()
	for len(origami.instructions) > 0 {
		origami = origami.nextFold()
	}

	origami.print()
	fmt.Println("day 13-2:", "see above", "(secret code)")
}
