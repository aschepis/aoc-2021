package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type SignalPattern struct {
	signals []string
	digits  []string
}

type SegmentMapping struct {
	top, topLeft, topRight, middle, bottomLeft, bottomRight, bottom string
}

func loadSignalPatterns() []SignalPattern {
	patterns := []SignalPattern{}

	file, _ := os.Open("day8.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		signalAndDigits := strings.Split(line, " | ")
		patterns = append(patterns, SignalPattern{
			signals: strings.Split(signalAndDigits[0], " "),
			digits:  strings.Split(signalAndDigits[1], " "),
		})
	}

	return patterns
}

func stringOverlap(a, b string) string {
	overlapMap := map[rune]bool{}

	for i := 0; i < len(a); i++ {
		for j := 0; j < len(b); j++ {
			if a[i] == b[j] {
				overlapMap[rune(a[i])] = true
			}
		}
	}

	out := ""
	for k, _ := range overlapMap {
		out = fmt.Sprintf("%s%s", out, string(k))
	}
	return out
}

func Day8_1() {
	uniqueDigitCount := 0

	for _, pattern := range loadSignalPatterns() {
		for _, digit := range pattern.digits {
			// 2 segments == 1
			// 3 segments == 7
			// 4 segments == 4
			// 7 segments == 8
			if len(digit) == 2 || len(digit) == 3 || len(digit) == 4 || len(digit) == 7 {
				uniqueDigitCount++
			}
		}
	}
	fmt.Println("day 8-1:", uniqueDigitCount, "(occurences of 1,4,7,8)")
}

func Day8_2() {
	signalPatterns := loadSignalPatterns()
	sum := 0

	// first go through and build the segment mapping
	for _, pattern := range signalPatterns {
		// start by analyzing signals
		letterCounts := map[string]int{"a": 0, "b": 0, "c": 0, "d": 0, "e": 0, "f": 0, "g": 0}
		for _, signal := range pattern.signals {
			for _, r := range signal {
				letterCounts[string(r)]++
			}
		}

		mapping := SegmentMapping{}
		for k, count := range letterCounts {
			if count == 6 {
				// this must be the top segment
				mapping.topLeft = k
			} else if count == 4 {
				// must be bottom left
				mapping.bottomLeft = k
			} else if count == 9 {
				// must be bottom right
				mapping.bottomRight = k
			} else if count == 7 {
				mapping.middle = fmt.Sprintf("%s%s", mapping.middle, k)
				mapping.bottom = fmt.Sprintf("%s%s", mapping.bottom, k)
			} else if count == 8 {
				mapping.topRight = fmt.Sprintf("%s%s", mapping.topRight, k)
				mapping.top = fmt.Sprintf("%s%s", mapping.top, k)
			}
		}

		// fmt.Printf("%+v\n", letterCounts)
		// fmt.Println(pattern.signals)
		// fmt.Printf("Segment Mapping: %+v\n", mapping)

		numstr := ""
		for _, digit := range pattern.digits {

			switch len(digit) {
			case 2:
				// 2 segments == 1
				numstr = fmt.Sprintf("%s%s", numstr, "1")
			case 3:
				// 3 segments == 7
				numstr = fmt.Sprintf("%s%s", numstr, "7")
			case 4:
				// 4 segments == 4
				numstr = fmt.Sprintf("%s%s", numstr, "4")
			case 5:
				// 5 segments == 2, 3, 5
				if strings.Contains(digit, mapping.bottomLeft) {
					// the bottom left segment is unique and among the options here only "2" will have
					// it set.
					numstr = fmt.Sprintf("%s%s", numstr, "2")
				} else if strings.Contains(digit, string(mapping.topRight[0])) && strings.Contains(digit, string(mapping.topRight[1])) {
					// of 3 and 5, only 3 has the top right segment activated. Since the frequency of the top right is the
					// same as the frequency of the top (which both have set) we just need to see if this digit has both
					// the top right possibilities set (thus has top and top right set)
					numstr = fmt.Sprintf("%s%s", numstr, "3")
				} else {
					// 3 is the only choice left!
					numstr = fmt.Sprintf("%s%s", numstr, "5")
				}
			case 6:
				// 6 segments == 0, 6, or 9
				hasTopRight1 := strings.Contains(digit, string(mapping.topRight[0]))
				hasTopRight2 := strings.Contains(digit, string(mapping.topRight[1]))
				if (hasTopRight1 && !hasTopRight2) || (!hasTopRight1 && hasTopRight2) {
					// top and top right share the same frequency. both 0 and 9 have
					// both of these set. 6 only has one of these set so if that is the case
					// then this must be 6
					numstr = fmt.Sprintf("%s%s", numstr, "6")
				} else if strings.Contains(digit, mapping.bottomLeft) {
					// of the remaining values 0 and 9 only 0 has bottom left set
					numstr = fmt.Sprintf("%s%s", numstr, "0")
				} else {
					numstr = fmt.Sprintf("%s%s", numstr, "9")
				}
			case 7:
				// 7 segments == 8
				numstr = fmt.Sprintf("%s%s", numstr, "8")
			}
		}
		// fmt.Println(numstr)
		num, e := strconv.Atoi(numstr)
		if e != nil {
			log.Fatal(e)
		}
		sum += num
	}

	fmt.Println("day 8-2:", sum, "(sum of values)")
}
