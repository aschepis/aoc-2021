package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	svg "github.com/ajstarks/svgo"
)

type Line struct {
	from Point
	to   Point
}

func (l *Line) project() []Point {
	points := []Point{}

	x := l.from.x
	y := l.from.y

	for x != l.to.x || y != l.to.y {
		points = append(points, Point{x: x, y: y})
		if x < l.to.x {
			x++
		} else if x > l.to.x {
			x--
		}

		if y < l.to.y {
			y++
		} else if y > l.to.y {
			y--
		}
	}

	points = append(points, l.to)

	return points
}

func (l *Line) overlapsWith(other Line) bool {
	// wow i know there is a math way to do this but i am brainfarting.
	// let's do it the stupid way.
	proj := l.project()
	projOther := other.project()

	for i := 0; i < len(proj); i++ {
		for j := 0; j < len(projOther); j++ {
			if proj[i].x == projOther[j].x && proj[i].y == projOther[j].y {
				return true
			}
		}
	}

	return false
}

func (l *Line) isDiagonal() bool {
	return l.from.x != l.to.x && l.from.y != l.to.y
}

func loadVents() []Line {
	lines := []Line{}

	file, _ := os.Open("day5.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, " -> ")
		from := strings.Split(parts[0], ",")
		to := strings.Split(parts[1], ",")
		x1, _ := strconv.Atoi(from[0])
		y1, _ := strconv.Atoi(from[1])
		x2, _ := strconv.Atoi(to[0])
		y2, _ := strconv.Atoi(to[1])
		lines = append(lines, Line{
			from: Point{x: x1, y: y1},
			to:   Point{x: x2, y: y2},
		})
	}

	return lines
}

func renderVents() {
	outFile, err := os.OpenFile("/tmp/vents.svg", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer outFile.Close()

	vents := loadVents()
	maxX := 0
	maxY := 0

	for _, vent := range vents {
		if vent.from.x > maxX {
			maxX = vent.from.x
		}
		if vent.to.x > maxX {
			maxX = vent.to.x
		}
		if vent.from.y > maxY {
			maxY = vent.from.y
		}
		if vent.to.y > maxY {
			maxY = vent.to.y
		}
	}

	width := maxX
	height := maxY
	canvas := svg.New(outFile)
	canvas.Start(width, height)

	for _, vent := range vents {
		canvas.Line(vent.from.x, vent.from.y, vent.to.x, vent.to.y, "stroke:red")
	}

	canvas.End()
}

func Day5_1() {
	vents := loadVents()
	overlaps := 0
	maxX := 0
	maxY := 0

	for _, vent := range vents {
		if vent.from.x > maxX {
			maxX = vent.from.x
		}
		if vent.to.x > maxX {
			maxX = vent.to.x
		}
		if vent.from.y > maxY {
			maxY = vent.from.y
		}
		if vent.to.y > maxY {
			maxY = vent.to.y
		}
	}

	width := maxX + 1
	height := maxY + 1

	board := make([]int, (width)*height)
	for _, vent := range vents {
		if vent.isDiagonal() {
			continue
		}
		points := vent.project()
		for _, point := range points {
			board[point.y*width+point.x]++
		}
	}

	for y := 0; y < height; y++ {
		for x := 0; x < height; x++ {
			if board[y*width+x] > 1 {
				overlaps++
			}
		}
	}

	fmt.Println("day 5-1:", overlaps, "(# of overlapping vent points horizontal/vertical)")
}

func Day5_2() {
	vents := loadVents()
	overlaps := 0
	maxX := 0
	maxY := 0

	for _, vent := range vents {
		if vent.from.x > maxX {
			maxX = vent.from.x
		}
		if vent.to.x > maxX {
			maxX = vent.to.x
		}
		if vent.from.y > maxY {
			maxY = vent.from.y
		}
		if vent.to.y > maxY {
			maxY = vent.to.y
		}
	}

	width := maxX + 1
	height := maxY + 1

	board := make([]int, (width)*height)
	for _, vent := range vents {
		points := vent.project()
		for _, point := range points {
			board[point.y*width+point.x]++
		}
	}

	for y := 0; y < height; y++ {
		for x := 0; x < height; x++ {
			if board[y*width+x] > 1 {
				overlaps++
			}
		}
	}

	fmt.Println("day 5-2:", overlaps, "(# of overlapping vent points)")
}
