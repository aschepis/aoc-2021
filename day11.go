package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type Octopi struct {
	Energy [][]int
}

func loadOctopi() Octopi {
	file, _ := os.Open("day11.txt")
	defer file.Close()

	octo := Octopi{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		column := []int{}
		for _, b := range line {
			r := string(b)
			i, _ := strconv.Atoi(r)
			column = append(column, i)
		}
		octo.Energy = append(octo.Energy, column)
	}
	return octo
}

func (o *Octopi) validNeighbors(p Point) []Point {
	potentialNeighbors := []Point{
		// above
		{x: -1, y: -1},
		{x: 0, y: -1},
		{x: 1, y: -1},
		// same row
		{x: -1, y: 0},
		{x: 1, y: 0},
		// below
		{x: -1, y: 1},
		{x: 0, y: 1},
		{x: 1, y: 1},
	}

	valid := []Point{}
	for _, np := range potentialNeighbors {
		absolutePoint := Point{x: np.x + p.x, y: np.y + p.y}
		if absolutePoint.x >= 0 && absolutePoint.y >= 0 && absolutePoint.x < len(o.Energy[0]) && absolutePoint.y < len(o.Energy) {
			valid = append(valid, absolutePoint)
		}
	}

	return valid
}

func (o *Octopi) print() {
	for y := 0; y < len(o.Energy); y++ {
		for x := 0; x < len(o.Energy[y]); x++ {
			fmt.Printf("%d", o.Energy[y][x])
		}
		fmt.Printf("\n")
	}
}

func (o *Octopi) incrementNeighbors(p Point) {
	neighbors := o.validNeighbors(p)
	for _, n := range neighbors {
		o.Energy[n.y][n.x]++
	}
}

func (o *Octopi) nextStep() (int, int) {
	// simulate flashes until none remain
	flashed := map[Point]bool{}
	totalFlashes := 0
	stop := false
	for stop == false {
		stepFlashes := 0
		for y := 0; y < len(o.Energy); y++ {
			for x := 0; x < len(o.Energy[y]); x++ {
				p := Point{x: x, y: y}
				if o.Energy[y][x] >= 9 && flashed[p] != true {
					stepFlashes++
					flashed[Point{x: x, y: y}] = true
					o.incrementNeighbors(p)
				}
			}
		}

		totalFlashes += stepFlashes
		if stepFlashes == 0 {
			stop = true
		}
	}

	// reset energy values for flashed octopi
	for y := 0; y < len(o.Energy); y++ {
		for x := 0; x < len(o.Energy[y]); x++ {
			if o.Energy[y][x] >= 9 {
				o.Energy[y][x] = 0
			}
		}
	}

	// increase all energy by 1
	for y := 0; y < len(o.Energy); y++ {
		for x := 0; x < len(o.Energy[y]); x++ {
			if flashed[Point{x: x, y: y}] != true {
				o.Energy[y][x]++
			}
		}
	}

	return totalFlashes, len(flashed)
}

func Day11_1() {
	octopi := loadOctopi()

	totalFlashes := 0
	for i := 0; i < 100; i++ {
		// octopi.print()
		// fmt.Println("")
		flashes, _ := octopi.nextStep()
		totalFlashes += flashes
		// fmt.Println(i, totalFlashes)
	}
	fmt.Println("day 11-1:", totalFlashes, "(total flashes)")
}

func Day11_2() {
	octopi := loadOctopi()
	totalOctopi := len(octopi.Energy) * len(octopi.Energy[0])
	i := 0
	stop := false
	for stop != true {
		// octopi.print()
		// fmt.Println("")
		_, flashed := octopi.nextStep()
		if flashed == totalOctopi {
			stop = true
		}
		i++
	}
	fmt.Println("day 11-2:", i, "(first synchronized flash)")
}
