package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func parseIntRange(str string) IntRange {
	parts := strings.Split(str[2:], "..")
	min, _ := strconv.Atoi(parts[0])
	max, _ := strconv.Atoi(parts[1])
	return IntRange{min: min, max: max}
}
func loadTrenchCoords() CoordinateRange {
	file, _ := os.Open("day17.txt")
	defer file.Close()

	crange := CoordinateRange{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, ", ")
		crange.x = parseIntRange(parts[0])
		crange.y = parseIntRange(parts[1])
	}

	return crange
}

type Submarine struct {
	p Point
}

type Probe struct {
	p                    Point
	velocityX, velocityY int
}

func (p *Probe) launch(velx, vely int) {
	p.velocityX = velx
	p.velocityY = vely
}

type TrenchSim struct {
	submarine Submarine
	probe     Probe
	target    CoordinateRange
}

func (s *TrenchSim) draw() {
	// draw the sim, surrounded by
	center := s.probe.p

	height := 30
	width := 80

	yoffset := 15 / 2
	xoffset := 40 / 2

	fmt.Println(s)
	fmt.Println("center:", center, "height:", height, "width:", width, "yoffset:", yoffset, "xoffset:", xoffset)
	for y := 0; y < height; y++ {
		realy := center.y + yoffset - y
		for x := 0; x < width; x++ {
			realx := x + center.x - xoffset
			if realy == 0 && realx == 0 {
				fmt.Printf("S")
			} else if realx == s.probe.p.x && realy == s.probe.p.y {
				fmt.Printf("#")
			} else if realx >= s.target.x.min && realx <= s.target.x.max && realy >= s.target.y.min && realy <= s.target.y.max {
				fmt.Printf("T")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Printf(" %4d\n", realy)
	}
}

func (s *TrenchSim) isSuccess() bool {
	return s.probe.p.x >= s.target.x.min && s.probe.p.x <= s.target.x.max && s.probe.p.y >= s.target.y.min && s.probe.p.y <= s.target.y.max
}

func (s *TrenchSim) isFailure() bool {
	return s.probe.p.y < s.target.y.min
}

func (s *TrenchSim) simulateRound() {
	s.probe.p.x += s.probe.velocityX
	s.probe.p.y += s.probe.velocityY

	if s.probe.velocityX > 0 {
		s.probe.velocityX -= 1
	} else if s.probe.velocityX < 0 {
		s.probe.velocityX += 1
	}

	s.probe.velocityY -= 1
}

func Day17_1() {
	crange := loadTrenchCoords()

	maxY := math.MinInt64

	// figure out start x and y based on where target is (don't go backwards if
	// it can never succeed)
	for launchx := -10000; launchx < 10000; launchx++ {
		for launchy := -10000; launchy < 10000; launchy++ {
			simMaxY := math.MinInt64
			sim := TrenchSim{submarine: Submarine{p: Point{x: 0, y: 0}}, probe: Probe{p: Point{x: 0, y: 0}}, target: crange}
			// targetWidth := int(math.Abs(float64(sim.target.x.max - sim.target.x.min)))
			// distanceToTarget := sim.probe.p.x + (sim.target.x.min + targetWidth/2)
			// fmt.Println("targetWidth:", targetWidth, "distance:", distanceToTarget, "launchx:", launchx, "launchy:", launchy)
			sim.probe.launch(launchx, launchy)

			round := 0
			for !sim.isFailure() && !sim.isSuccess() {
				// fmt.Println("round", round)
				// sim.draw()
				sim.simulateRound()
				if sim.probe.p.y > simMaxY {
					simMaxY = sim.probe.p.y
				}

				// fmt.Scanln()
				round++
			}
			// sim.draw()
			// fmt.Println("==================================")
			if sim.isSuccess() {
				if simMaxY > maxY {
					maxY = simMaxY
				}
				fmt.Println("success -", launchx, launchy)
			} else if sim.isFailure() {
				// fmt.Println("failure")
			} else {
				// fmt.Println("out of time")
			}
			// fmt.Println("==================================")
		}
	}
	fmt.Println("day 17-1:", maxY, "(max y)")
}

func Day17_2() {
	crange := loadTrenchCoords()

	maxY := math.MinInt64
	successCount := 0
	// figure out start x and y based on where target is (don't go backwards if
	// it can never succeed)
	for launchx := -10000; launchx < 10000; launchx++ {
		for launchy := -10000; launchy < 10000; launchy++ {
			sim := TrenchSim{submarine: Submarine{p: Point{x: 0, y: 0}}, probe: Probe{p: Point{x: 0, y: 0}}, target: crange}
			if (sim.target.x.min > 0 && launchx < 0) || (sim.target.x.min < 0 && launchx > 0) {
				continue
			}
			if (sim.target.x.max > 0 && launchx > sim.target.x.max) || (sim.target.x.max < 0 && launchx < sim.target.x.max) {
				continue
			}
			// if launchy < sim.target.y.max {
			// 	continue
			// }
			simMaxY := math.MinInt64
			// targetWidth := int(math.Abs(float64(sim.target.x.max - sim.target.x.min)))
			// distanceToTarget := sim.probe.p.x + (sim.target.x.min + targetWidth/2)
			// fmt.Println("targetWidth:", targetWidth, "distance:", distanceToTarget, "launchx:", launchx, "launchy:", launchy)
			fmt.Println(launchx, launchy)
			sim.probe.launch(launchx, launchy)

			round := 0
			for !sim.isFailure() && !sim.isSuccess() {
				// fmt.Println("round", round)
				// sim.draw()
				sim.simulateRound()
				if sim.probe.p.y > simMaxY {
					simMaxY = sim.probe.p.y
				}

				// fmt.Scanln()
				round++
			}
			// sim.draw()
			// fmt.Println("==================================")
			if sim.isSuccess() {
				if simMaxY > maxY {
					maxY = simMaxY
				}
				fmt.Println("success -", launchx, launchy)
				successCount++
			} else if sim.isFailure() {
				// fmt.Println("failure")
			} else {
				// fmt.Println("out of time")
			}
			// fmt.Println("==================================")
		}
	}
	fmt.Println("day 17-2:", successCount, "(success count)")
}

func SimOne() {
	crange := loadTrenchCoords()

	launchx := 0
	launchy := 66

	sim := TrenchSim{submarine: Submarine{p: Point{x: 0, y: 0}}, probe: Probe{p: Point{x: 0, y: 0}}, target: crange}
	// targetWidth := int(math.Abs(float64(sim.target.x.max - sim.target.x.min)))
	// distanceToTarget := sim.probe.p.x + (sim.target.x.min + targetWidth/2)
	// fmt.Println("targetWidth:", targetWidth, "distance:", distanceToTarget, "launchx:", launchx, "launchy:", launchy)
	sim.probe.launch(launchx, launchy)

	round := 0
	for !sim.isFailure() && !sim.isSuccess() {
		fmt.Println("round", round)
		fmt.Println("x:", sim.probe.p.x, "y", sim.probe.p.y, "velX:", sim.probe.velocityX, "velY:", sim.probe.velocityY)
		sim.draw()
		sim.simulateRound()

		fmt.Scanln()
		round++
	}
	sim.draw()
	fmt.Println("==================================")
	if sim.isSuccess() {
		fmt.Println("success -", launchx, launchy)
	} else if sim.isFailure() {
		fmt.Println("failure")
	} else {
		fmt.Println("out of time")
	}
	fmt.Println("==================================")
}
