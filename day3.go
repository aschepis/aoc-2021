package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func diagnosticValues() ([]int64, int) {
	values := []int64{}

	file, err := os.Open("day3.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	bits := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if err != nil {
			log.Fatal(err)
		}

		bits = len(line)
		val, _ := strconv.ParseInt(line, 2, 64)
		values = append(values, val)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return values, bits
}

// will always return 1 or 0
func mostCommonBit(values []int64, bitpos int) int {
	bitCount := 0
	for _, val := range values {
		bitval := (int64(1<<bitpos) & val) >> bitpos
		if bitval == 0 {
			bitCount--
		} else {
			bitCount++
		}
	}
	if bitCount < 0 {
		return 0
	}
	return 1
}

func Day3_1() {
	values, bits := diagnosticValues()

	gamma := 0
	epsilon := 0
	for i := bits - 1; i >= 0; i-- {
		commonBit := mostCommonBit(values, i)

		if commonBit == 1 {
			// 1 is more common
			gamma += 1 << i
		} else {
			epsilon += 1 << i
		}
	}

	fmt.Println("day 3-1:", gamma*epsilon, "(power consumption)")
}

func narrowDiagnostic(values []int64, bits int, mostCommon bool) int64 {
	for len(values) > 1 {
		for i := bits - 1; i >= 0; i-- {
			commonBit := mostCommonBit(values, i)
			if !mostCommon {
				if commonBit == 1 {
					commonBit = 0
				} else {
					commonBit = 1
				}
			}

			newValues := []int64{}
			for _, val := range values {
				if (1<<i)&val>>i == int64(commonBit) {
					newValues = append(newValues, val)
				}
			}
			values = newValues
			if len(values) == 1 {
				break
			}
		}
	}

	return values[0]
}
func Day3_2() {
	values, bits := diagnosticValues()
	o2Rating := narrowDiagnostic(values, bits, true)

	values, bits = diagnosticValues()
	co2Rating := narrowDiagnostic(values, bits, false)

	fmt.Println("day 3-2:", o2Rating*co2Rating, "(life support rating)")
}
