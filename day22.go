package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	"strconv"
)

func loadReactorInstructions() []ReactorInstruction {
	file, _ := os.Open("day22.txt")
	defer file.Close()

	// on x=10..12,y=10..12,z=10..12
	lineRe := regexp.MustCompile(`(?P<onoff>[onf]+) x=(?P<x1>-?\d+)..(?P<x2>-?\d+),y=(?P<y1>-?\d+)..(?P<y2>-?\d+),z=(?P<z1>-?\d+)..(?P<z2>-?\d+)`)
	instructions := []ReactorInstruction{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		instruction := ReactorInstruction{}
		matches := matchStringMap(lineRe, line)
		if matches["onoff"] == "on" {
			instruction.turnOn = true
		}

		x1, _ := strconv.Atoi(matches["x1"])
		x2, _ := strconv.Atoi(matches["x2"])
		y1, _ := strconv.Atoi(matches["y1"])
		y2, _ := strconv.Atoi(matches["y2"])
		z1, _ := strconv.Atoi(matches["z1"])
		z2, _ := strconv.Atoi(matches["z2"])

		if x2 < x1 {
			t := x2
			x2 = x1
			x1 = t
		}

		if y2 < y1 {
			t := y2
			y2 = y1
			y1 = t
		}

		if z2 < z1 {
			t := z2
			z2 = z1
			z1 = t
		}

		instruction.coordRange.x = IntRange{min: x1, max: x2}
		instruction.coordRange.y = IntRange{min: y1, max: y2}
		instruction.coordRange.z = IntRange{min: z1, max: z2}

		instructions = append(instructions, instruction)
	}
	return instructions
}

type ReactorInstruction struct {
	coordRange CoordinateRange3D
	turnOn     bool
}

type Reactor struct {
	size      int
	processed []MetaCube
}

func makeReactor(size int) *Reactor {
	r := &Reactor{}
	r.size = size
	return r
}

func (r *Reactor) validInstruction(i ReactorInstruction) bool {
	// an instruction is invalid if it is COMPLETELY outside of the bounds
	// of the reactor
	minBound := -r.size
	maxBound := r.size
	intRange := IntRange{min: minBound, max: maxBound}
	rangeCube := CoordinateRange3D{x: intRange, y: intRange, z: intRange}

	return rangeCube.overlaps(i.coordRange)
}

func (r *Reactor) validPoint(p Point3D) bool {
	minBound := -r.size
	maxBound := r.size
	return p.x >= minBound && p.x <= maxBound &&
		p.y >= minBound && p.y <= maxBound &&
		p.z >= minBound && p.z <= maxBound
}

type Cube CoordinateRange3D

func max(a, b int) int {
	if b > a {
		return b
	}
	return a
}

func min(a, b int) int {
	if b < a {
		return b
	}
	return a
}

func (c Cube) overlapCube(other Cube) Cube {
	return Cube{
		x: IntRange{min: max(c.x.min, other.x.min), max: min(c.x.max, other.x.max)},
		y: IntRange{min: max(c.y.min, other.y.min), max: min(c.y.max, other.y.max)},
		z: IntRange{min: max(c.z.min, other.z.min), max: min(c.z.max, other.z.max)},
	}
}

func (c Cube) contains(other Cube) bool {
	return c.x.min <= other.x.min && c.x.max >= other.x.max &&
		c.y.min <= other.y.min && c.y.max >= other.y.max &&
		c.z.min <= other.z.min && c.z.max >= other.z.max
}

func (c Cube) overlaps(other Cube) bool {
	return CoordinateRange3D(c).overlaps(CoordinateRange3D(other))
}

// carve "other" out of "c". returns a list of cubes that
// constitute "c" with "other" cut out of it.
func (c Cube) carveOut(other Cube) []Cube {
	cubes := []Cube{}

	// find the overlapping area
	overlap := c.overlapCube(other)
	fmt.Println(overlap)

	// if      c == x:{11,13}, y:{11,13}, z:{11,13}
	// and other == x:{10,12}, y:{10,12}, z:{10,12}
	// we need to carve out the 11-12 area and create 6 new cubes that
	// surround "other"

	//left, right, up, down, front back

	// create a set of new cubes that are adjacent to "other"
	return cubes
}

func (c Cube) volume() int {
	x := c.x.max - c.x.min + 1
	y := c.y.max - c.y.min + 1
	z := c.z.max - c.z.min + 1
	return x * y * z
}

type MetaCube struct {
	cube     Cube
	positive bool
}

func (r *Reactor) performInstruction(i ReactorInstruction) {
	if !r.validInstruction(i) {
		return
	}

	cube := Cube(i.coordRange)

	nextProcessed := []MetaCube{}
	for _, previous := range r.processed {
		nextProcessed = append(nextProcessed, previous)
		if previous.cube.overlaps(cube) {
			overlapCube := previous.cube.overlapCube(cube)
			positiveSpace := true
			if previous.positive != i.turnOn {
				positiveSpace = i.turnOn
			} else {
				positiveSpace = !i.turnOn
			}
			metaOverlapCube := MetaCube{cube: overlapCube, positive: positiveSpace}
			// fmt.Println("appending space (positive:", metaOverlapCube.positive, ") space with volume", metaOverlapCube.cube.volume())
			nextProcessed = append(nextProcessed, metaOverlapCube)
		}
	}

	if i.turnOn {
		nextProcessed = append(nextProcessed, MetaCube{cube: cube, positive: i.turnOn})
	}

	r.processed = nextProcessed
	// fmt.Println("new total:", r.totalVolume())
}

func (r *Reactor) totalVolume() int {
	v := 0
	for _, r := range r.processed {
		if r.positive {
			v += r.cube.volume()
		} else {
			v -= r.cube.volume()
		}
	}
	return v
}

func Day22_1() {
	instructions := loadReactorInstructions()
	reactor := makeReactor(50)
	for _, i := range instructions {
		reactor.performInstruction(i)
	}
	fmt.Println("day 22-1:", reactor.totalVolume(), "(total volume)")
}

func Day22_2() {
	instructions := loadReactorInstructions()
	reactor := makeReactor(math.MaxInt64)
	for _, i := range instructions {
		reactor.performInstruction(i)
	}
	fmt.Println("day 22-2:", reactor.totalVolume(), "(total volume)")
}
