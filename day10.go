package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

var syntaxScores map[rune]int = map[rune]int{
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137,
}

var completionScores map[rune]int = map[rune]int{
	'(': 1,
	'[': 2,
	'{': 3,
	'<': 4,
}

var closersAndOpeners map[rune]rune = map[rune]rune{
	')': '(',
	']': '[',
	'}': '{',
	'>': '<',
}

type Program struct {
	code string
}

func (p *Program) closers() string {
	closers := ""
	for k := range closersAndOpeners {
		closers = fmt.Sprintf("%s%c", closers, k)
	}
	return closers
}
func (p *Program) openers() string {
	openers := ""
	for _, v := range closersAndOpeners {
		openers = fmt.Sprintf("%s%c", openers, v)
	}
	return openers
}

func (p *Program) completionScore() int {
	// assumes incomplete program
	openers := p.openers()
	stack := RuneStack{}
	for _, b := range p.code {
		r := rune(b)
		if strings.Contains(openers, string(r)) {
			stack.Push(r)
		} else {
			stack.Pop()
		}
	}

	score := 0
	for stack.Len() > 0 {
		r, _ := stack.Pop()
		if strings.Contains(openers, string(r)) {
			score = (score * 5) + completionScores[r]
		}
	}

	return score
}
func (p *Program) Compile() (error, int) {
	openers := p.openers()
	stack := RuneStack{}
	for _, b := range p.code {
		r := rune(b)
		if strings.Contains(openers, string(r)) {
			stack.Push(r)
		} else {
			opener, err := stack.Pop()
			if err != nil {
				// incomplete program. determine completion score
				return fmt.Errorf("incomplete program"), p.completionScore()
			}
			expectedOpener := closersAndOpeners[r]
			if expectedOpener != opener {
				return fmt.Errorf("invalid syntax"), syntaxScores[r]
			}
		}
	}
	// potentially incomplete program
	if stack.Len() > 0 {
		return fmt.Errorf("incomplete program"), p.completionScore()
	}
	return nil, 0
}

func loadPrograms() []Program {
	file, _ := os.Open("day10.txt")
	defer file.Close()

	programs := []Program{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		programs = append(programs, Program{code: line})
	}
	return programs
}

func Day10_1() {
	programs := loadPrograms()

	totalScore := 0
	for _, p := range programs {
		err, score := p.Compile()
		if err != nil && err.Error() == "invalid syntax" {
			totalScore += score
		}
	}

	fmt.Println("day 10-1:", totalScore, "(syntax error score)")
}

func Day10_2() {
	programs := loadPrograms()

	completionScores := []int{}
	for _, p := range programs {
		err, score := p.Compile()
		if err != nil && err.Error() == "incomplete program" {
			completionScores = append(completionScores, score)
		}
	}

	sort.Ints(completionScores)
	autocompleteScore := completionScores[len(completionScores)/2]

	fmt.Println("day 10-2:", autocompleteScore, "(autocomplete score)")
}
