package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

type Heightmap struct {
	heights [][]int
}

func (h Heightmap) String() string {
	out := ""
	split := ""
	for _, row := range h.heights {
		out = fmt.Sprintf("%s%s%+v", out, split, row)
		split = "\n"
	}
	return out
}

func (h Heightmap) neighbors(p Point) []Point {
	neighbors := []Point{
		{x: p.x - 1, y: p.y},
		{x: p.x + 1, y: p.y},
		{x: p.x, y: p.y - 1},
		{x: p.x, y: p.y + 1},
	}

	validNeighbors := []Point{}
	for _, n := range neighbors {
		if n.x >= 0 && n.x < len(h.heights[0]) && n.y >= 0 && n.y < len(h.heights) {
			validNeighbors = append(validNeighbors, n)
		}
	}
	return validNeighbors
}

func (h Heightmap) isLowPoint(p Point) bool {
	height := h.heights[p.y][p.x]

	for _, n := range h.neighbors(p) {
		nh := h.heights[n.y][n.x]
		if height >= nh {
			return false
		}
	}
	return true
}

func (h Heightmap) lowCoords() []Point {
	coords := map[Point]bool{}
	for y := 0; y < len(h.heights); y++ {
		for x := 0; x < len(h.heights[y]); x++ {
			p := Point{x: x, y: y}
			if h.isLowPoint(p) {
				coords[p] = true
			}
		}
	}

	lows := []Point{}
	for k := range coords {
		lows = append(lows, k)
	}
	return lows
}

func (h Heightmap) lowPoints() []int {
	lows := []int{}
	for _, coord := range h.lowCoords() {
		lows = append(lows, h.heights[coord.y][coord.x])
	}
	return lows
}

func (h Heightmap) fillBasin(p Point, flooded map[Point]bool) int {
	if h.heights[p.y][p.x] == 9 || flooded[p] {
		return 0
	}

	size := 1
	flooded[p] = true
	for _, n := range h.neighbors(p) {
		size += h.fillBasin(n, flooded)
	}

	return size
}

func (h Heightmap) basinSizes() []int {
	sizes := []int{}

	lows := h.lowCoords()
	for _, bottom := range lows {
		size := h.fillBasin(bottom, map[Point]bool{})
		sizes = append(sizes, size)
	}

	return sizes
}

func loadHeightmap() Heightmap {
	file, _ := os.Open("day9.txt")
	defer file.Close()

	heightmap := Heightmap{heights: [][]int{}}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		row := []int{}
		for _, b := range line {
			height, _ := strconv.Atoi(string(b))
			row = append(row, height)
		}
		heightmap.heights = append(heightmap.heights, row)
	}
	return heightmap
}

func Day9_1() {
	heightmap := loadHeightmap()
	lowPoints := heightmap.lowPoints()
	sum := 0
	for _, p := range lowPoints {
		sum += p + 1 // risk = height + 1
	}
	fmt.Println("day 9-1:", sum, "(sum of risks)")
}

func Day9_2() {
	heightmap := loadHeightmap()
	basins := heightmap.basinSizes()
	sort.Ints(basins)

	l := len(basins)
	top3Product := basins[l-1] * basins[l-2] * basins[l-3]

	fmt.Println("day 9-2:", top3Product, "(product of 3 largest basins)")
}
