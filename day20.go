package main

import (
	"bufio"
	"fmt"
	"os"
)

type ImageEnhancementAlgorithm string

type Image struct {
	pixels [][]rune
}

func (i *Image) pixelBlock(x, y int) []rune {
	relativeCoords := []Point{
		{-1, -1},
		{0, -1},
		{1, -1},
		{-1, 0},
		{0, 0},
		{1, 0},
		{-1, 1},
		{0, 1},
		{1, 1},
	}

	pixels := []rune{}
	for _, p := range relativeCoords {
		dark := false
		if y+p.y < 0 || y+p.y > len(i.pixels)-1 {
			dark = true
		} else if x+p.x < 0 || x+p.x > len(i.pixels)-1 {
			dark = true
		}

		if dark {
			pixels = append(pixels, '.')
		} else {
			pixels = append(pixels, i.pixels[y+p.y][x+p.x])
		}
	}
	return pixels
}

func pixelBlockToValue(block []rune) int {
	val := 0
	if len(block) != 9 {
		panic("wtf")
	}
	for i, r := range block {
		if r == '#' {
			val += 1 << (8 - i)
		}
	}
	return val
}
func (i *Image) enhance(algorithm ImageEnhancementAlgorithm) Image {
	newImage := Image{}

	for y := -1; y < len(i.pixels)+1; y++ {
		row := []rune{}
		for x := -1; x < len(i.pixels[0])+1; x++ {
			pixelBlock := i.pixelBlock(x, y)
			blockValue := pixelBlockToValue(pixelBlock)
			enhancedPixel := rune(algorithm[blockValue])
			row = append(row, enhancedPixel)
		}
		newImage.pixels = append(newImage.pixels, row)
	}
	return newImage
}

func (i *Image) pixelCount() int {
	count := 0
	for y := 0; y < len(i.pixels); y++ {
		for x := 0; x < len(i.pixels[y]); x++ {
			r := rune(i.pixels[y][x])
			if r == '#' {
				count++
			}
		}
	}
	return count
}

func (i *Image) String() string {
	s := ""
	for y := 0; y < len(i.pixels); y++ {
		for x := 0; x < len(i.pixels[y]); x++ {
			s = fmt.Sprintf("%s%s", s, string(i.pixels[y][x]))
		}
		s = fmt.Sprintf("%s\n", s)
	}
	return s
}

func loadImageAndAlgorithm() (Image, ImageEnhancementAlgorithm) {
	file, _ := os.Open("day20.txt")
	defer file.Close()

	algorithm := ImageEnhancementAlgorithm("")
	image := Image{}

	scanner := bufio.NewScanner(file)
	loadingImage := false
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			loadingImage = true
			continue
		}

		if loadingImage {
			row := []rune{}
			for _, r := range line {
				row = append(row, r)
			}
			image.pixels = append(image.pixels, row)
		} else {
			algorithm = ImageEnhancementAlgorithm(line)
		}
	}

	return image, algorithm
}

func Day20_1() {
	image, algorithm := loadImageAndAlgorithm()
	fmt.Println(algorithm)
	fmt.Println("")
	fmt.Println(image.String())

	newImage := image.enhance(algorithm)
	fmt.Println("numPixels:", newImage.pixelCount())
	finalImage := newImage.enhance(algorithm)
	numPixels := finalImage.pixelCount()
	fmt.Println("final numPixels:", numPixels)

	fmt.Println("day 20-1:", numPixels, "(pixel count)")
}

func Day20_2() {
	fmt.Println("day 20-2:", 0, "()")
}
