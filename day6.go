package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Generation struct {
	days, size int
}

func loadLanternfish() []int {
	fish := []int{}

	file, _ := os.Open("day6.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		fishStr := strings.Split(line, ",")
		for _, s := range fishStr {
			val, _ := strconv.Atoi(s)
			fish = append(fish, val)
		}
	}

	return fish
}

func spawnFish(fish []int, totalDays int) int64 {
	generations := []Generation{
		{days: 0, size: 0},
		{days: 1, size: 0},
		{days: 2, size: 0},
		{days: 3, size: 0},
		{days: 4, size: 0},
		{days: 5, size: 0},
		{days: 6, size: 0},
		{days: 7, size: 0},
		{days: 8, size: 0},
	}

	for _, f := range fish {
		generations[f].size++
	}
	for day := 0; day < totalDays; day++ {
		var currCount int64 = 0
		for _, generation := range generations {
			currCount += int64(generation.size)
		}
		numGenerations := len(generations)
		// fmt.Println("day", day, "fish:", currCount, "generations:", numGenerations)
		for i := 0; i < numGenerations; i++ {
			if generations[i].size > 0 {
				if generations[i].days == 0 {
					generations = append(generations, Generation{days: 8, size: generations[i].size})
					generations[i].days = 6
				} else {
					generations[i].days--
				}
			}
		}

		// consolidate generations
		newGenerations := []Generation{
			{days: 0, size: 0},
			{days: 1, size: 0},
			{days: 2, size: 0},
			{days: 3, size: 0},
			{days: 4, size: 0},
			{days: 5, size: 0},
			{days: 6, size: 0},
			{days: 7, size: 0},
			{days: 8, size: 0},
		}
		for _, gen := range generations {
			newGenerations[gen.days].size += gen.size
		}

		generations = newGenerations
	}

	var totalFish int64 = 0
	for _, g := range generations {
		totalFish += int64(g.size)
	}
	return totalFish
}

func Day6_1() {
	fish := loadLanternfish()
	// for i := 0; i < 80; i++ {
	// 	newFish := []int{}
	// 	for _, f := range fish {
	// 		if f == 0 {
	// 			newFish = append(newFish, 6, 8)
	// 		} else {
	// 			newFish = append(newFish, f-1)
	// 		}
	// 	}
	// 	fish = newFish
	// }
	// fmt.Println("day 6-1:", len(fish), "(lanternfish)")
	fmt.Println("day 6-1:", spawnFish(fish, 80), "(lanternfish in 80 days)")
}

func Day6_2() {
	fish := loadLanternfish()
	fmt.Println("day 6-1:", spawnFish(fish, 256), "(lanternfish in 256 days)")
}
