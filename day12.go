package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type GraphNode struct {
	name  string
	edges []*GraphNode
}

func (n *GraphNode) isLower() bool {
	return strings.ToLower(n.name) == n.name
}

func (n *GraphNode) isStart() bool {
	return n.name == "start"
}
func (n *GraphNode) isEnd() bool {
	return n.name == "end"
}

func loadGraph() (*GraphNode, *GraphNode) {
	file, _ := os.Open("day12.txt")
	defer file.Close()

	nodes := map[string]*GraphNode{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "-")
		from := parts[0]
		to := parts[1]

		// add nodes that don't exist
		if nodes[from] == nil {
			nodes[from] = &GraphNode{name: from, edges: []*GraphNode{}}
		}

		if nodes[to] == nil {
			nodes[to] = &GraphNode{name: to, edges: []*GraphNode{}}
		}

		// add edges
		nodes[to].edges = append(nodes[to].edges, nodes[from])
		nodes[from].edges = append(nodes[from].edges, nodes[to])
	}

	return nodes["start"], nodes["end"]
}

func anyRevisits(visits map[string]int) bool {
	for k, v := range visits {
		if v > 1 && strings.ToLower(k) == k {
			// fmt.Println("  have already revisted", k)
			return true
		}
	}
	return false
}

func (n *GraphNode) pathsToInternal(end *GraphNode, takeYourTime bool, visited map[string]int, current string) []string {
	paths := []string{}

	if end.name == n.name {
		return []string{end.name}
	}

	visited[n.name]++
	current = fmt.Sprintf("%s,%s", current, n.name)

	// find valid next steps
	for _, next := range n.edges {
		if next.isStart() {
			continue
		}

		if takeYourTime {
			if !next.isLower() || visited[next.name] == 0 {
				// do nothing
			} else if (next.isLower() && visited[next.name] > 0) && !anyRevisits(visited) {
				// fmt.Println("allowing a revisit to", next.name)
				// do nothing
			} else {
				continue
			}
		} else {
			if visited[next.name] > 0 && next.isLower() {
				// can't go back to start lowercase node
				continue
			}
		}

		// fmt.Println(n.name, "->", next.name)

		visitedClone := map[string]int{}
		copyMap(visited, &visitedClone)

		subpaths := next.pathsToInternal(end, takeYourTime, visitedClone, current)

		for _, p := range subpaths {
			paths = append(paths, fmt.Sprintf("%s,%s", n.name, p))
		}
	}
	current = current[0 : len(current)-len(n.name)-1]
	return paths
}
func (n *GraphNode) pathsTo(end *GraphNode, takeYourTime bool) []string {
	return n.pathsToInternal(end, takeYourTime, map[string]int{}, "")
}

func Day12_1() {
	start, end := loadGraph()
	paths := start.pathsTo(end, false)
	fmt.Println("day 12-1:", len(paths), "(valid paths)")
}

func Day12_2() {
	start, end := loadGraph()
	paths := start.pathsTo(end, true)
	// for _, path := range paths {
	// 	fmt.Println(path)
	// }
	fmt.Println("day 12-2:", len(paths), "(valid paths with 1 small re-traversal)")
}
