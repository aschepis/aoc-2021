package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Player struct {
	position int
	score    int
}

type DeterministicDie struct {
	nextRoll, numRolls int
}

func (d *DeterministicDie) roll() int {
	d.nextRoll++
	if d.nextRoll > 100 {
		d.nextRoll = 1
	}
	d.numRolls++
	return d.nextRoll
}

func loadPlayers() []Player {
	file, _ := os.Open("day21.txt")
	defer file.Close()

	players := make([]Player, 2)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, ": ")
		startingPos, _ := strconv.Atoi(parts[1])
		if strings.Contains(parts[0], "Player 1") {
			players[0] = Player{position: startingPos - 1, score: 0}
		} else {
			players[1] = Player{position: startingPos - 1, score: 0}
		}
	}

	return players
}

func Day21_1() {
	d := DeterministicDie{}
	players := loadPlayers()
	fmt.Println(players)

	winningScore := 1000
	playerIndex := 0
	for players[0].score < winningScore && players[1].score < winningScore {
		val := d.roll() + d.roll() + d.roll()
		players[playerIndex].position = (players[playerIndex].position + val) % 10
		players[playerIndex].score += players[playerIndex].position + 1
		playerIndex = (playerIndex + 1) % 2
	}
	// playerIndex should indicate the loser
	answer := players[playerIndex].score * d.numRolls
	fmt.Println("day 21-1:", answer, "(score calc)")
}

type Universe struct {
	players      []Player
	playerIndex  int
	winningScore int
}

func (u *Universe) hasWinner() bool {
	return u.players[0].score > u.winningScore || u.players[1].score > u.winningScore
}

func (u *Universe) winnerIndex() int {
	if u.players[0].score > u.winningScore {
		return 0
	}
	return 1
}

func (u *Universe) split(roll int) *Universe {
	newu := u.copy()
	newu.players[newu.playerIndex].position = (newu.players[newu.playerIndex].position + roll) % 10
	newu.players[newu.playerIndex].score += newu.players[newu.playerIndex].position + 1
	newu.playerIndex = (newu.playerIndex + 1) % 2
	return newu
}

func (u *Universe) id() uint64 {
	id := uint64(0)
	id += uint64(u.players[0].position << 32)
	id += uint64(u.players[0].score << 28)
	id += uint64(u.players[1].position << 24)
	id += uint64(u.players[1].score << 20)
	id += uint64(u.playerIndex << 16)
	return id
}

func (u *Universe) equal(b *Universe) bool {
	return u.players[0].position == b.players[0].position &&
		u.players[0].score == b.players[0].score &&
		u.players[1].position == b.players[1].position &&
		u.playerIndex == b.playerIndex
}

func (u *Universe) copy() *Universe {
	return &Universe{
		players: []Player{
			{position: u.players[0].position, score: u.players[0].score},
			{position: u.players[1].position, score: u.players[1].score},
		},
		playerIndex:  u.playerIndex,
		winningScore: u.winningScore,
	}
}

type UniverseNode struct {
	u    *Universe
	next *UniverseNode
}

func (n *UniverseNode) length() int {
	sum := 0
	node := n
	for node != nil {
		sum++
		node = node.next
	}
	return sum
}

func Day21_2() {
	players := loadPlayers()
	wins := []uint64{0, 0}

	list := &UniverseNode{
		u: &Universe{players: players, playerIndex: 0, winningScore: 21},
	}

	possibilities := [][]int{
		{1, 1, 1},
		{1, 1, 2},
		{1, 1, 3},
		// {1, 2, 1},
		// {1, 2, 2},
		{1, 2, 3},
		// {1, 3, 1},
		// {1, 3, 2},
		{1, 3, 3},
		// {2, 1, 1},
		// {2, 1, 2},
		// {2, 1, 3},
		// {2, 2, 1},
		// {2, 2, 2},
		// {2, 2, 3},
		// {2, 3, 1},
		// {2, 3, 2},
		{2, 3, 3},
		// {3, 1, 1},
		// {3, 1, 2},
		// {3, 1, 3},
		// {3, 2, 1},
		// {3, 2, 2},
		// {3, 2, 3},
		// {3, 3, 1},
		// {3, 3, 2},
		{3, 3, 3},
	}

	cache := map[uint64]int{}
	cacheHits := 0
	cacheMiss := 0
	for list != nil {
		fmt.Println("loop:", list.length(), "- cache:", cacheHits, "/", cacheMiss)

		node := list
		var newList *UniverseNode = nil
		var curr *UniverseNode = nil
		for node != nil {
			if winner, found := cache[node.u.id()]; found {
				// fmt.Println("cache hit")
				wins[winner]++
				cacheHits++
				node = node.next
				continue
			}
			cacheMiss++
			if node.u.hasWinner() {
				wins[node.u.winnerIndex()]++
				cache[node.u.id()] = node.u.winnerIndex()
			} else {
				for _, p := range possibilities {
					roll := p[0] + p[1] + p[2]
					newu := node.u.split(roll)
					if newList == nil {
						newList = &UniverseNode{u: newu}
						curr = newList
					} else {
						curr.next = &UniverseNode{u: newu}
						curr = curr.next
					}
				}
			}
			node = node.next
		}
		list = newList
	}

	winnerWins := uint64(0)
	if wins[0] > wins[1] {
		winnerWins = wins[0]
	} else {
		winnerWins = wins[1]
	}

	fmt.Println("day 21-2:", winnerWins, "(quantum wins)")
}
