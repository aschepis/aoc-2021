package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func loadCrabSubs() []int {
	crabSubs := []int{}

	file, _ := os.Open("day7.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		crabSubsStr := strings.Split(line, ",")
		for _, s := range crabSubsStr {
			val, _ := strconv.Atoi(s)
			crabSubs = append(crabSubs, val)
		}
	}

	return crabSubs
}

func Day7_1() {
	crabSubs := loadCrabSubs()
	sort.Ints(crabSubs)
	median := crabSubs[len(crabSubs)/2]

	requiredFuel := 0
	for _, v := range crabSubs {
		requiredFuel += int(math.Abs(float64(v - median)))
	}
	// move to the median
	fmt.Println("day 7-1:", requiredFuel, "(required fuel)")
}

func Day7_2() {
	crabSubs := loadCrabSubs()
	sort.Ints(crabSubs)

	min := math.MaxInt32
	max := 0

	for _, v := range crabSubs {
		if v > max {
			max = v
		}
		if v < min {
			min = v
		}
	}

	minFuel := math.MaxInt32
	for i := min; i <= max; i++ {
		requiredFuel := 0
		alignOn := i
		for _, v := range crabSubs {
			movement := int(math.Abs(float64(v - alignOn)))
			fuelSpent := (movement * (movement + 1)) / 2
			// fmt.Println(v, avg, movement, "fualSpent:", fuelSpent)
			requiredFuel += fuelSpent
		}
		// fmt.Println("alignOn:", alignOn, "requiredFuel:", requiredFuel)
		if requiredFuel < minFuel {
			minFuel = requiredFuel
		}
	}
	fmt.Println("day 7-2:", minFuel, "(required fuel - factorial)")
}
