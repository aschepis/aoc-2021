module github.com/aschepis/aoc

go 1.16

require (
	github.com/RyanCarrier/dijkstra v1.1.0 // indirect
	github.com/ajstarks/svgo v0.0.0-20211024235047-1546f124cd8b
	github.com/fatih/color v1.13.0 // indirect
	github.com/google/pprof v0.0.0-20211204230040-2007db6d4f53 // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20211126204342-3ad08eb09c01 // indirect
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/pkg/profile v1.6.0 // indirect
	github.com/uber/go-torch v0.0.0-20181107071353-86f327cc820e // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486 // indirect
)
