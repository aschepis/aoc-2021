package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func depths() []int {
	values := []int{}

	file, err := os.Open("day1.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		val, err := strconv.Atoi(line)
		if err != nil {
			log.Fatal(err)
		}

		values = append(values, val)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return values
}

func sumInts(vals []int) int {
	sum := 0
	for _, val := range vals {
		sum += val
	}

	return sum
}

func Day1_1() {
	depths := depths()

	increases := 0
	previousDepth := -1

	for _, depth := range depths {
		if previousDepth != -1 && depth > previousDepth {
			increases++
		}
		previousDepth = depth
	}

	fmt.Println("day 1-1:", increases, "(depth increases)")
}

func Day1_2() {
	depths := depths()

	increases := 0
	previousDepth := -1

	for i := range depths {
		start := i
		end := i + 2
		if i < 2 {
			start = 0
			end = i
		}
		slice := depths[start : end+1]

		depth := sumInts(slice)
		if previousDepth != -1 && depth > previousDepth {
			increases++
		}
		previousDepth = depth
	}

	fmt.Println("day 1-2:", increases, "(depth window increases)")
}
