package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type SnailfishEquationNode struct {
	parent *SnailfishEquationNode
	left   *SnailfishEquationNode
	right  *SnailfishEquationNode
	val    *int
}

func (n *SnailfishEquationNode) str() string {
	if n.val != nil {
		return fmt.Sprintf("%d", *n.val)
	} else {
		if n.left != nil && n.right != nil {
			return fmt.Sprintf("[%s,%s]", n.left.str(), n.right.str())
		} else if n.left != nil {
			return fmt.Sprintf("[%s]", n.left.str())
		} else {
			return fmt.Sprintf("[%s]", n.right.str())
		}
	}
}

func (n *SnailfishEquationNode) String() string {
	return fmt.Sprint(n.str())
}

func (n *SnailfishEquationNode) printNode(parent int, i *int, f *os.File) {
	(*i)++
	nodeId := (*i) + 1
	if parent > 0 {
		fmt.Fprintf(f, "  n%d -> n%d\n", parent, nodeId)
	}

	if n.val == nil {
		fmt.Fprintf(f, "  n%d [label=\"\"]\n", nodeId)
	} else {
		fmt.Fprintf(f, "  n%d [label=\"%d\"]\n", nodeId, *n.val)
	}

	if n.left != nil {
		n.left.printNode(nodeId, i, f)
	}
	if n.right != nil {
		n.right.printNode(nodeId, i, f)
	}
}

func printEquation(head *SnailfishEquationNode, filename string) {
	f, _ := os.Create(filename)
	defer f.Close()
	fmt.Fprintf(f, "digraph \"equation\" {\n")
	id := 0
	head.printNode(0, &id, f)
	id += 50
	fmt.Fprintf(f, "  n%d [label=\"%s\"]\n", id, head.String())
	fmt.Fprintf(f, "}\n")
}

func parseSnailfishEquation(str string) *SnailfishEquationNode {
	var equation *SnailfishEquationNode = nil
	var curr *SnailfishEquationNode = nil

	for i := 0; i < len(str); i++ {
		r := rune(str[i])
		switch r {
		case '[':
			if curr == nil {
				curr = &SnailfishEquationNode{}
				equation = curr
			} else {
				node := &SnailfishEquationNode{parent: curr}
				if curr.left == nil {
					// fmt.Println("down left")
					curr.left = node
				} else {
					// fmt.Println("down right")
					curr.right = node
				}
				curr = node
			}
		case ']':
			// fmt.Println("up")
			curr = curr.parent
		case ',':
			// keep going
			// fmt.Println("continue")
		default:
			// a number
			numstr := ""
			for str[i] != ']' && str[i] != ',' {
				numstr = fmt.Sprintf("%s%s", numstr, string(str[i]))
				i++
			}
			i--
			val, _ := strconv.Atoi(numstr)
			node := &SnailfishEquationNode{parent: curr, val: &val}
			if curr.left == nil {
				// fmt.Println("val left")
				curr.left = node
			} else {
				// fmt.Println("val right")
				curr.right = node
			}
		}
	}

	return equation
}

func (n *SnailfishEquationNode) validate() {
	if n.left != nil {
		if n.left.parent != n {
			panic(fmt.Sprintln("bad left node!", n))
		}
		n.left.validate()
	}

	if n.right != nil {
		if n.right.parent != n {
			panic(fmt.Sprintln("bad right node!", n))
		}
		n.right.validate()
	}
}

func (n *SnailfishEquationNode) magnitude() uint64 {
	sum := uint64(0)

	if n.val != nil {
		// fmt.Println("  returning", *n.val)
		return uint64(*n.val)
	}

	if n.left != nil {
		sum += uint64(3) * n.left.magnitude()
	}
	if n.right != nil {
		sum += uint64(2) * n.right.magnitude()
	}
	// fmt.Println("sum for", n, "=", sum)
	return sum
}

func (n *SnailfishEquationNode) add(node *SnailfishEquationNode) *SnailfishEquationNode {
	parent := &SnailfishEquationNode{left: n, right: node}
	n.parent = parent
	node.parent = parent
	return parent
}

func (n *SnailfishEquationNode) nextLeftNode() *SnailfishEquationNode {
	// in this case we have a pair of plain values:
	// [3,2] and we traverse up and to the left

	// fmt.Println("first left of", n)

	// we need to look for something left in
	// the tree, but we can't go back down
	// our own path.
	cursor := n.parent
	// fmt.Println("cursor", cursor)
	prev := n
	// fmt.Println("starting point", cursor)
	leftOnce := false
	for cursor != nil && cursor.val == nil {
		if cursor.left == prev {
			prev = cursor
			cursor = cursor.parent
		} else if leftOnce && cursor.right != nil {
			prev = cursor
			cursor = cursor.right
		} else if cursor.left != nil {
			prev = cursor
			cursor = cursor.left
			leftOnce = true
		}
	}

	// if cursor.val == nil {
	// 	return nil
	// }

	return cursor
}

func (n *SnailfishEquationNode) nextRightNode() *SnailfishEquationNode {
	// in this case we have a pair of plain values:
	// [3,2] and we traverse up and to the right
	// fmt.Println("first right of", n)
	cursor := n.parent
	prev := n
	// fmt.Println("starting point", cursor)
	// we need to look for something right in
	// the tree, but we can't go back down
	// our own path.
	rightOnce := false
	for cursor != nil && cursor.val == nil {
		if cursor.right == prev {
			prev = cursor
			cursor = cursor.parent
		} else if rightOnce && cursor.left != nil {
			prev = cursor
			cursor = cursor.left
		} else if cursor.right != nil {
			rightOnce = true
			prev = cursor
			cursor = cursor.right
		}
	}

	// if cursor.val == nil {
	// 	return nil
	// }

	return cursor
}

func (n *SnailfishEquationNode) reduce(depth int, explode bool) bool {
	if explode && depth == 4 && n.val == nil {
		// fmt.Println("four deep. time to explode:", n)

		isLeftNode := n.parent.left == n
		nextLeftNode := n.nextLeftNode()
		nextRightNode := n.nextRightNode()

		if nextLeftNode != nil {
			// [[n.p.p[n.p[7,[n->[8,4],9]]]]]
			// [[newparent->[15,[0,13]]]]
			if nextRightNode != nil {
				// we have a left and right node
				leftVal := *nextLeftNode.val + *n.left.val
				rightVal := *nextRightNode.val + *n.right.val
				zeroVal := 0

				// add values to "next" nodes
				nextLeftNode.val = &leftVal
				nextRightNode.val = &rightVal

				zeroNode := &SnailfishEquationNode{parent: n.parent, val: &zeroVal}
				if isLeftNode {
					n.parent.left = zeroNode
				} else {
					n.parent.right = zeroNode
				}
			} else {
				zeroVal := 0
				leftVal := *nextLeftNode.val + *n.left.val

				nextLeftNode.val = &leftVal
				zeroNode := &SnailfishEquationNode{parent: n.parent, val: &zeroVal}
				if isLeftNode {
					n.parent.left = zeroNode
				} else {
					n.parent.right = zeroNode
				}
			}
			return true
		} else {
			// parent should explode to zero
			// [[[1,2],2]] -> [[0,3],2]
			zeroVal := 0
			rightSum := *nextRightNode.val + *n.right.val

			nextRightNode.val = &rightSum
			zeroNode := &SnailfishEquationNode{parent: n.parent, val: &zeroVal}
			if isLeftNode {
				n.parent.left = zeroNode
			} else {
				n.parent.right = zeroNode
			}
			return true
		}
	}

	if n.left != nil {
		didSomething := n.left.reduce(depth+1, explode)
		if didSomething {
			return true
		}
	}
	if n.right != nil {
		didSomething := n.right.reduce(depth+1, explode)
		if didSomething {
			return true
		}
	}

	if !explode && n.left != nil && n.left.val != nil && *n.left.val >= 10 {
		// fmt.Println("greater than 10 on left.. lets split")
		// fmt.Println(n)

		val := *n.left.val
		leftVal := 0
		rightVal := 0
		if val%2 == 1 {
			leftVal = val / 2
			rightVal = val/2 + 1
		} else {
			leftVal = val / 2
			rightVal = val / 2
		}

		n.left.val = nil
		n.left.left = &SnailfishEquationNode{parent: n.left, val: &leftVal}
		n.left.right = &SnailfishEquationNode{parent: n.left, val: &rightVal}
		return true
	}

	if n.left != nil {
		didSomething := n.left.reduce(depth+1, explode)
		if didSomething {
			return true
		}
	}
	if n.right != nil {
		didSomething := n.right.reduce(depth+1, explode)
		if didSomething {
			return true
		}
	}

	if !explode && n.right != nil && n.right.val != nil && *n.right.val >= 10 {
		// fmt.Println("greater than 10 on right.. lets split")
		// fmt.Println(n)

		val := *n.right.val
		leftVal := 0
		rightVal := 0
		if val%2 == 1 {
			leftVal = val / 2
			rightVal = val/2 + 1
		} else {
			leftVal = val / 2
			rightVal = val / 2
		}

		n.right.val = nil
		n.right.left = &SnailfishEquationNode{parent: n.right, val: &leftVal}
		n.right.right = &SnailfishEquationNode{parent: n.right, val: &rightVal}
		return true
	}

	if n.left != nil {
		didSomething := n.left.reduce(depth+1, explode)
		if didSomething {
			return true
		}
	}
	if n.right != nil {
		didSomething := n.right.reduce(depth+1, explode)
		if didSomething {
			return true
		}
	}

	return false
}

func loadSnailfishNumbers() []*SnailfishEquationNode {
	file, _ := os.Open("day18.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	equations := []*SnailfishEquationNode{}
	for scanner.Scan() {
		line := scanner.Text()
		equation := parseSnailfishEquation(line)
		equations = append(equations, equation)
	}

	return equations
}

func Day18_1() {
	equations := loadSnailfishNumbers()

	reduceRounds := 0
	for len(equations) > 1 {
		// fmt.Println(equations[0], "+", equations[1])
		neq := equations[0].add(equations[1])
		// fmt.Println("=", neq)

		fmt.Println(neq)
		printEquation(neq, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))

		updated := neq.reduce(0, true)
		if !updated {
			updated = neq.reduce(0, false)
		}
		reduceRounds++
		// fmt.Println("reduced round 1 - ", neq)
		fmt.Println(neq)
		printEquation(neq, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))
		neq.validate()

		round := 0
		for updated {
			updated = neq.reduce(0, true)
			if !updated {
				updated = neq.reduce(0, false)
			}
			round++
			reduceRounds++
			// fmt.Println("reduced round ", round, "-", neq)
			fmt.Println(neq)
			printEquation(neq, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))
			neq.validate()
		}

		fmt.Println("fully reduced", "-", neq)
		printEquation(neq, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))
		reduceRounds++

		newEq := []*SnailfishEquationNode{neq}
		if len(equations) >= 2 {
			newEq = append(newEq, equations[2:]...)
		}
		equations = newEq
	}
	final := equations[0]
	fmt.Println(final)
	reduceRounds++
	printEquation(final, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))

	updated := final.reduce(0, true)
	if !updated {
		updated = final.reduce(0, false)
	}
	reduceRounds++
	fmt.Println(final)
	printEquation(final, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))

	for updated {
		updated = final.reduce(0, true)
		if !updated {
			updated = final.reduce(0, false)
		}
		reduceRounds++
		fmt.Println(final)
		printEquation(final, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))
	}

	// fmt.Println("reduced round ", round, "-", neq)
	fmt.Println(final)
	printEquation(final, fmt.Sprintf("/tmp/reduced-%05d.dot", reduceRounds))

	printEquation(final, "/tmp/final.dot")
	fmt.Println("final:", final)
	fmt.Println("day 18-1:", final.magnitude(), "(magnitude)")
}

func loopReduce(eq *SnailfishEquationNode, print bool) {
	// initial reduce
	rounds := 0

	updated := eq.reduce(0, true)
	if !updated {
		updated = eq.reduce(0, false)
	}
	if print {
		printEquation(eq, fmt.Sprintf("/tmp/reduced-%05d.dot", rounds))
		rounds++
	}

	// loop reduce
	for updated {
		updated = eq.reduce(0, true)
		if !updated {
			updated = eq.reduce(0, false)
		}
		if print {
			printEquation(eq, fmt.Sprintf("/tmp/reduced-%05d.dot", rounds))
			rounds++
		}
	}
}
func Day18_2() {
	maxMagnitude := uint64(0)
	equations := loadSnailfishNumbers()
	numEquations := len(equations)
	for i := 0; i < numEquations; i++ {
		for j := 0; j < numEquations; j++ {
			if i == j {
				continue
			}

			if equations[i].String() == "[[[[7,9],[4,9]],[7,[7,5]]],[6,[[1,6],[8,7]]]]" &&
				equations[j].String() == "[[[3,[7,6]],[9,9]],[[8,9],[[6,2],[3,4]]]]" {
				eq2 := loadSnailfishNumbers()
				neq2 := equations[i].add(eq2[j])
				loopReduce(neq2, true)
				fmt.Println("done")
			}
			//

			equations := loadSnailfishNumbers()
			neq := equations[i].add(equations[j])
			loopReduce(neq, false)

			// we have a reduced number.. get its magnitude
			mag := neq.magnitude()
			if mag > maxMagnitude {
				fmt.Println("new largest:", i, j, "-", mag)
				maxMagnitude = mag
			}
		}
	}
	fmt.Println("day 18-2:", maxMagnitude, "(largest magnitude)")
}
