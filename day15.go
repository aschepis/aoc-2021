package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"

	"github.com/RyanCarrier/dijkstra"
)

type CaveNode struct {
	parent  *CaveNode
	pos     Point
	f, g, h float64
}

type CaveStats struct {
	openSize   int
	closedSize int
	iterations int
}

func loadCave() [][]int {
	file, _ := os.Open("day15.txt")
	defer file.Close()

	cave := [][]int{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		row := []int{}
		for _, r := range line {
			val, _ := strconv.Atoi(string(r))
			row = append(row, val)
		}
		cave = append(cave, row)
	}

	return cave
}

func loadCave5x() [][]int {
	cave := loadCave()

	factor := 5
	rows := len(cave)
	cols := len(cave[0])
	totalRows := rows * factor
	totalColumns := cols * factor
	bigCave := make([][]int, totalRows)
	for i := 0; i < totalRows; i++ {
		bigCave[i] = make([]int, totalColumns)
	}

	for by := 0; by < factor; by++ {
		for bx := 0; bx < factor; bx++ {
			for y := 0; y < len(cave); y++ {
				for x := 0; x < len(cave[y]); x++ {
					heightHere := cave[y][x] + by + bx
					if heightHere > 9 {
						heightHere -= 9
					}
					cx := (by * rows) + y
					cy := (bx * cols) + x
					bigCave[cy][cx] = heightHere
				}
			}
		}
	}
	return bigCave
}

func spliceNodes(a []*CaveNode, index int) []*CaveNode {
	ret := make([]*CaveNode, len(a)-1)
	j := 0
	for i, n := range a {
		if i != index {
			ret[j] = n
			j++
		}
	}
	return ret
}

func neighborNodes(p Point, cave [][]int) []Point {
	neighbors := []Point{
		{x: p.x - 1, y: p.y},
		{x: p.x + 1, y: p.y},
		{x: p.x, y: p.y - 1},
		{x: p.x, y: p.y + 1},
	}

	validNeighbors := []Point{}
	for _, n := range neighbors {
		if n.x >= 0 && n.x < len(cave[0]) && n.y >= 0 && n.y < len(cave) {
			validNeighbors = append(validNeighbors, n)
		}
	}
	return validNeighbors
}

type PriorityQueueNode struct {
	val      Point
	priority float64
}

type PriorityQueue struct {
	values []PriorityQueueNode
}

func (p *PriorityQueue) get() Point {
	node := p.values[len(p.values)-1]
	p.values = p.values[0 : len(p.values)-1]
	return node.val
}

func (p *PriorityQueue) put(point Point, priority float64) {
	p.values = append(p.values, PriorityQueueNode{val: point, priority: priority})
	sort.Slice(p.values, func(i, j int) bool {
		return p.values[i].priority > p.values[j].priority
	})
}

func (p *PriorityQueue) empty() bool {
	return len(p.values) == 0
}

func dijkstra2(from, to Point, cave [][]int) []Point {
	frontier := PriorityQueue{}
	frontier.put(from, 0)
	cameFrom := map[Point]Point{}
	costSoFar := map[Point]float64{}
	for !frontier.empty() {
		q := frontier.get()
		if q == to {
			path := []Point{q}

			for true {
				if q == from {
					break
				}
				if n, found := cameFrom[q]; found {
					path = append(path, n)
					q = n
				} else {
					panic("what!?")
				}
			}
			return path
		}

		neighbors := neighborNodes(q, cave)
		for _, n := range neighbors {
			cost := costSoFar[q] + float64(cave[q.y][q.x])
			if _, found := cameFrom[n]; !found {
				costSoFar[n] = cost
				priority := cost //math.Abs(float64(to.x-n.x)) + math.Abs(float64(to.y-n.y)) // + float64(cave[n.y][n.x])
				frontier.put(n, priority)
				cameFrom[n] = q
			}
		}
	}
	return []Point{{}}
}

func astar2(from, to Point, cave [][]int) []Point {
	start := CaveNode{parent: nil, pos: from}
	end := CaveNode{parent: nil, pos: to}

	open := []*CaveNode{&start}
	closed := []*CaveNode{}
	i := 0
	for len(open) > 0 {
		q := open[0]
		idx := 0
		for i, n := range open {
			if n.f < q.f {
				q = n
				idx = i
			}
		}
		open = spliceNodes(open, idx)

		neighbors := neighborNodes(q.pos, cave)
		children := []*CaveNode{}
		for _, n := range neighbors {
			children = append(children, &CaveNode{parent: q, pos: n})
		}

		for _, c := range children {
			if c.pos == end.pos {
				path := []Point{}
				last := c
				for last != nil {
					path = append(path, last.pos)
					last = last.parent
				}
				return path
			}

			c.g = q.g + float64(cave[c.pos.y][c.pos.x])
			c.h = 0 //math.Sqrt(math.Abs(float64(c.pos.x-end.pos.x)) + math.Abs(float64(c.pos.y-end.pos.y)))
			c.f = c.g + c.h

			hasLowerOpen := false
			for _, n := range open {
				if n.pos == c.pos && n.f < c.f {
					hasLowerOpen = true
					break
				}
			}

			if hasLowerOpen {
				continue
			}

			hasLowerClosed := false
			for _, n := range closed {
				if n.pos == c.pos && n.f < c.f {
					hasLowerClosed = true
					break
				}
			}

			if hasLowerClosed {
				continue
			}

			open = append(open, c)
		}

		if i%25000 == 0 {
			path := []Point{}
			last := q
			for last != nil {
				path = append(path, last.pos)
				last = last.parent
			}
			stats := CaveStats{
				openSize:   len(open),
				closedSize: len(closed),
				iterations: i,
			}
			printPathResult(path, cave, fmt.Sprintf("-%d", i), &stats)
			fmt.Println("dumped stats", i)
		}
		i++
		closed = append(closed, q)
	}

	panic("no path!")
}

func astar(from, to Point, cave [][]int) []Point {
	start := CaveNode{parent: nil, pos: from}
	end := CaveNode{parent: nil, pos: to}

	open := make([]*CaveNode, len(cave)*len(cave))
	closed := make([]*CaveNode, len(cave)*len(cave))

	open = append(open, &start)

	i := 0
	for len(open) > 0 {
		i++
		curr := open[0]
		idx := 0
		for i, n := range open {
			if n.f < curr.f {
				curr = n
				idx = i
			}
		}

		// remove current from open list. add to closed
		open = spliceNodes(open, idx)
		closed = append(closed, curr)

		if curr.pos == end.pos {
			path := []Point{}
			for curr != nil {
				path = append(path, curr.pos)
				curr = curr.parent
			}
			// in reverse order from end -> start
			return path
		}

		neighbors := neighborNodes(curr.pos, cave)
		children := make([]*CaveNode, len(neighbors))
		for _, p := range neighbors {
			children = append(children, &CaveNode{
				parent: curr,
				pos:    p,
			})
		}

		// set f,g,h values
		for _, c := range children {
			inClosed := false
			for _, cl := range closed {
				if cl.pos == c.pos {
					inClosed = true
					break
				}
			}
			if inClosed {
				continue
			}

			c.g = curr.g + float64(cave[c.pos.y][c.pos.x]) //(cave[c.pos.y][c.pos.x] * len(cave))
			c.h = 0                                        //int(math.Pow(float64(c.pos.x-end.pos.x), 2)) + int(math.Pow(float64(c.pos.y-end.pos.y), 2))
			c.f = c.g + c.h

			// in open
			inOpened := false
			for _, o := range open {
				if c.pos == o.pos && c.g > o.g {
					inOpened = true
					break
				}
			}
			if !inOpened {
				open = append(open, c)
			}
		}

		printDebug(cave, curr, open, closed)

		if i%25000 == 0 {
			path := []Point{}
			for curr != nil {
				path = append(path, curr.pos)
				curr = curr.parent
			}
			stats := CaveStats{
				openSize:   len(open),
				closedSize: len(closed),
				iterations: i,
			}
			printPathResult(path, cave, fmt.Sprintf("-%d", i), &stats)
			fmt.Println("dumped stats", i)
		}
	}
	return []Point{}
}

func printDebug(cave [][]int, curr *CaveNode, open, closed []*CaveNode) {
	path := []Point{}
	for curr != nil {
		path = append(path, curr.pos)
		curr = curr.parent
	}

	f := os.Stdout
	for y := range cave {
		for x := range cave[y] {
			inPath := false
			for _, p := range path {
				if p.x == x && p.y == y {
					inPath = true
					break
				}
			}
			if inPath {
				fmt.Fprintf(f, "x")
			} else {
				// inClosed := false
				// p := Point{x: x, y: y}
				// for _, c := range closed {
				// 	if c.pos == p {
				// 		inClosed = true
				// 		break
				// 	}
				// }
				// if inClosed {
				// 	fmt.Fprintf(f, "c")
				// } else {
				fmt.Fprintf(f, "%d", cave[y][x])
				// }
			}
		}
		fmt.Fprintf(f, "\n")
	}
	fmt.Fprintf(f, "\n\n")
}
func printPathResult(path []Point, cave [][]int, suffix string, stats *CaveStats) {
	filename := fmt.Sprintf("/tmp/path%s.txt", suffix)
	f, _ := os.Create(filename)
	defer f.Close()
	// f := os.Stdout
	for y := range cave {
		for x := range cave[y] {
			inPath := false
			for _, p := range path {
				if p.x == x && p.y == y {
					inPath = true
					break
				}
			}
			if inPath {
				fmt.Fprintf(f, "x")
			} else {
				fmt.Fprintf(f, "%d", cave[y][x])
			}
		}
		fmt.Fprintf(f, "\n")
	}

	if stats != nil {
		fmt.Fprintf(f, "\nStats:\n")
		fmt.Fprintf(f, "Open: %d\n", stats.openSize)
		fmt.Fprintf(f, "Closed: %d\n", stats.closedSize)
		fmt.Fprintf(f, "Iterations: %d", stats.iterations)
	}
}

func solve(from, to Point, cave [][]int) int64 {
	graph := dijkstra.NewGraph()
	for y := 0; y < len(cave); y++ {
		for x := 0; x < len(cave[y]); x++ {
			vertexId := y*len(cave[y]) + x
			graph.AddVertex(vertexId)
		}
	}

	// add arcs
	for y := 0; y < len(cave); y++ {
		for x := 0; x < len(cave[y]); x++ {
			vertexId := y*len(cave[y]) + x
			neighbors := neighborNodes(Point{x: x, y: y}, cave)
			for _, n := range neighbors {
				nVertexId := n.y*len(cave[y]) + n.x
				graph.AddArc(vertexId, nVertexId, int64(cave[n.y][n.x]))
			}
		}
	}

	path, _ := graph.Shortest(0, (len(cave)*len(cave[0]))-1)
	return path.Distance
}
func Day15_1() {
	cave := loadCave()
	start := Point{x: 0, y: 0}
	end := Point{x: len(cave[0]) - 1, y: len(cave) - 1}

	// f, err := os.Create("/tmp/cpu.pprof")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// pprof.StartCPUProfile(f)
	// defer pprof.StopCPUProfile()

	// path := astar2(start, end, cave)
	// cost := 0
	// // fmt.Println(path)
	// printPathResult(path, cave, "", nil)
	// for _, p := range path[0 : len(path)-1] {
	// 	cost += cave[p.y][p.x]
	// }
	fmt.Println("day 15-1:", solve(start, end, cave), "(least risky path through cave)")
}

func Day15_2() {
	cave := loadCave5x()
	start := Point{x: 0, y: 0}
	end := Point{x: len(cave[0]) - 1, y: len(cave) - 1}

	// f, err := os.Create("/tmp/cpu.pprof")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// pprof.StartCPUProfile(f)
	// defer pprof.StopCPUProfile()

	// path := astar2(start, end, cave)
	// cost := 0
	// printPathResult(path, cave, "", nil)
	// for _, p := range path[0 : len(path)-1] {
	// 	cost += cave[p.y][p.x]
	// }
	fmt.Println("day 15-2:", solve(start, end, cave), "(least risky path through cave 5x)")
}
