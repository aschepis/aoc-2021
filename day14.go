package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strings"
)

func loadPolymerData() (string, map[string]rune) {
	file, _ := os.Open("day14.txt")
	defer file.Close()

	template := ""
	insertionRules := map[string]rune{}
	scanner := bufio.NewScanner(file)
	insertionSection := false
	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			insertionSection = true
		} else if insertionSection {
			parts := strings.Split(line, " -> ")
			if len(parts[1]) > 1 {
				panic(1)
			}
			insertionRules[parts[0]] = rune(parts[1][0])
		} else {
			template = line
		}
	}

	return template, insertionRules
}

func processRule(digraphCounts map[string]int64, rules map[string]rune) map[string]int64 {
	newCounts := map[string]int64{}
	for digraph, count := range digraphCounts {
		insertion := rules[digraph]
		left := fmt.Sprintf("%s%s", string(digraph[0]), string(insertion))
		right := fmt.Sprintf("%s%s", string(insertion), string(digraph[1]))
		newCounts[left] += count
		newCounts[right] += count
	}
	return newCounts
}

func polymerize(template string, rules map[string]rune, times int) int64 {
	digraphCounts := map[string]int64{}
	for i := 0; i < len(template)-1; i++ {
		digraphCounts[template[i:i+2]] = 1
	}

	for i := 0; i < times; i++ {
		digraphCounts = processRule(digraphCounts, rules)
		// fmt.Println(i, "-", digraphCounts)
	}

	charCounts := map[rune]int64{}
	for digraph, count := range digraphCounts {
		charCounts[rune(digraph[1])] += count
	}
	charCounts[rune(template[0])] += 1

	most := int64(0)
	least := int64(math.MaxInt64)
	for _, v := range charCounts {
		if v > most {
			most = v
		}
		if v < least {
			least = v
		}
	}

	return most - least
}

func Day14_1() {
	template, insertionRules := loadPolymerData()
	fmt.Println("day 14-1:", polymerize(template, insertionRules, 10), "(insertion rules 10 times)")
}

func Day14_2() {
	template, insertionRules := loadPolymerData()
	fmt.Println("day 14-2:", polymerize(template, insertionRules, 40), "(insertion rules 40 times)")
}
