package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type square struct {
	val    int
	marked bool
}

type board struct {
	// length=25 to represent a 5x5
	squares      []square
	disqualified bool
}

func (b *board) print() {
	fmt.Println(b)
}

func (b *board) isWinner() bool {
	// horizontal
	for y := 0; y < 5; y++ {
		markCount := 0
		for x := 0; x < 5; x++ {
			if b.squares[y*5+x].marked {
				markCount++
			}
		}
		if markCount == 5 {
			return true
		}
	}

	// vertical
	for x := 0; x < 5; x++ {
		markCount := 0
		for y := 0; y < 5; y++ {
			if b.squares[y*5+x].marked {
				markCount++
			}
		}
		if markCount == 5 {
			return true
		}
	}

	return false
}

func (b *board) mark(val int) {
	for i := 0; i < len(b.squares); i++ {
		if b.squares[i].val == val {
			b.squares[i].marked = true
		}
	}
}

func (b *board) unmarkedSum() int {
	sum := 0
	for i := 0; i < len(b.squares); i++ {
		if !b.squares[i].marked {
			sum += b.squares[i].val
		}
	}
	return sum
}

func boardsAndValues() ([]*board, []int) {
	boards := []*board{}
	values := []int{}

	file, _ := os.Open("day4.txt")
	defer file.Close()

	first := true
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if first {
			first = false
			strValues := strings.Split(line, ",")
			for _, str := range strValues {
				val, _ := strconv.Atoi(str)
				values = append(values, val)
			}
		} else {
			if len(line) == 0 {
				boards = append(boards, &board{})
			} else {
				currentBoard := boards[len(boards)-1]
				// add a line to the board.
				// strip whitespace to handle " 3 13 14 15 16" lines with leading whitespace
				line = strings.TrimSpace(line)
				// split on spaces
				lineNumbers := strings.Split(line, " ")
				for _, ns := range lineNumbers {
					// skip empty strings to handle case where single digit is in the middle e.g. "12 13  4 15 16"
					if len(ns) == 0 {
						continue
					}
					n, _ := strconv.Atoi(ns)
					currentBoard.squares = append(currentBoard.squares, square{val: n, marked: false})
				}
			}
		}
	}

	return boards, values
}

func Day4_1() {
	boards, values := boardsAndValues()

	var winner *board = nil
	lastVal := 0
	for _, val := range values {
		if winner != nil {
			break
		}
		for _, b := range boards {
			b.mark(val)
			if b.isWinner() {
				lastVal = val
				winner = b
				break
			}
		}
	}

	// we have a winner!
	fmt.Println("day 4-1:", winner.unmarkedSum()*lastVal, "(winner unmarked sum)")
}

func Day4_2() {
	boards, values := boardsAndValues()

	var winner *board = nil
	lastVal := 0
	for _, val := range values {
		for _, b := range boards {
			if !b.disqualified {
				b.mark(val)
				if b.isWinner() {
					b.disqualified = true
					lastVal = val
					winner = b
				}
			}
		}
	}

	// we have a winner!
	fmt.Println("day 4-2:", winner.unmarkedSum()*lastVal, "(last winner unmarked sum)")
}
