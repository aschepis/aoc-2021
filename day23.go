package main

import (
	"bufio"
	"crypto/sha256"
	"fmt"
	"math"
	"os"
	"sort"
)

type AmphipodKind int
type AmphipodHome int

const (
	Amber  AmphipodKind = 1
	Bronze AmphipodKind = 10
	Copper AmphipodKind = 100
	Desert AmphipodKind = 1000
)

// home columns (zero indexed)
const (
	A AmphipodHome = 3
	B AmphipodHome = 5
	C AmphipodHome = 7
	D AmphipodHome = 9
)

type Amphipod struct {
	pos  Point
	kind AmphipodKind
}

type Cave struct {
	amphipods []*Amphipod
	board     [][]rune
}

func loadAmphipodCave(long bool) Cave {
	filename := "day23.txt"
	if long {
		filename = "day23-2.txt"
	}
	file, _ := os.Open(filename)
	defer file.Close()

	cave := Cave{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		row := []rune{}
		line := scanner.Text()
		for i, r := range line {
			if r == 'A' || r == 'B' || r == 'C' || r == 'D' {
				amphi := &Amphipod{pos: Point{x: i, y: len(cave.board)}}
				switch r {
				case 'A':
					amphi.kind = Amber
				case 'B':
					amphi.kind = Bronze
				case 'C':
					amphi.kind = Copper
				case 'D':
					amphi.kind = Desert
				}
				cave.amphipods = append(cave.amphipods, amphi)
			}
			row = append(row, r)
		}
		cave.board = append(cave.board, row)
	}
	return cave
}

func (c *Cave) amphiIsHome(a *Amphipod) bool {
	return c.amphiWouldBeHome(a, a.pos)
}

func (c *Cave) amphiWouldBeHome(a *Amphipod, p Point) bool {
	wouldBeHome := true
	for y := p.y + 1; y < len(c.board)-1; y++ {
		if !wouldBeHome {
			return false
		}
		switch a.kind {
		case Amber:
			if p.x != int(A) || !(c.board[y][p.x] == 'A' || c.board[y][p.x] == '#') {
				wouldBeHome = false
			}
		case Bronze:
			if p.x != int(B) || !(c.board[y][p.x] == 'B' || c.board[y][p.x] == '#') {
				wouldBeHome = false
			}
		case Copper:
			if p.x != int(C) || !(c.board[y][p.x] == 'C' || c.board[y][p.x] == '#') {
				wouldBeHome = false
			}
		case Desert:
			if p.x != int(D) || !(c.board[y][p.x] == 'D' || c.board[y][p.x] == '#') {
				wouldBeHome = false
			}
		}
	}
	return wouldBeHome
}

func (c *Cave) isOrganized() bool {
	for y := 2; y < len(c.board)-1; y++ {
		if c.board[y][A] != 'A' || c.board[y][B] != 'B' || c.board[y][C] != 'C' || c.board[y][D] != 'D' {
			return false
		}
	}
	return true
}

func (c *Cave) String() string {
	str := ""
	for y := 0; y < len(c.board); y++ {
		for x := 0; x < len(c.board[y]); x++ {
			str = fmt.Sprintf("%s%s", str, string(c.board[y][x]))
		}
		if y != len(c.board)-1 {
			str = fmt.Sprintf("%s\n", str)
		}
	}
	return str
}

type Move struct {
	to     Point
	kind   AmphipodKind
	energy int
}

var potentialMoves []Point = []Point{
	{x: -1, y: 0},
	{x: 1, y: 0},
	{x: 0, y: -1},
	{x: 0, y: 1},
}

func (c *Cave) validMovesInner(a *Amphipod, depth int, seen map[Point]bool) []Move {
	moves := []Move{}

	if c.amphiIsHome(a) {
		return moves
	}

	// don't come back to this point again
	seen[a.pos] = true

	for _, m := range potentialMoves {
		x := a.pos.x + m.x
		y := a.pos.y + m.y

		// short circuit some work.
		if y == 0 || x == 0 || x > len(c.board[0]) || y > len(c.board)-1 {
			continue
		}

		point := Point{x: x, y: y}
		nextChar := c.board[y][x]

		// we have been here before.. don't add this point to the list or continue to search
		if _, found := seen[point]; found {
			continue
		}

		// we can only move into empty spaces
		if nextChar == '.' {
			// qualify this move as legal
			charBelow := c.board[y+1][x]
			// if we are in an endzone:
			if point.y > 1 {
				// make sure its the right one before we allow it!
				if (x == int(A) && a.kind == Amber) || (x == int(B) && a.kind == Bronze) ||
					(x == int(C) && a.kind == Copper) || (x == int(D) && a.kind == Desert) {
					// make sure it is at the bottom of the endzone or else it is not legal.
					aChar := 'A'
					switch a.kind {
					case Amber:
						aChar = 'A'
					case Bronze:
						aChar = 'B'
					case Copper:
						aChar = 'C'
					case Desert:
						aChar = 'D'
					}
					if charBelow == '#' || charBelow == aChar {
						// this is a valid move into the endzone.
						move := Move{to: point, kind: a.kind, energy: int(a.kind) * (depth + 1)}
						if c.amphiWouldBeHome(a, move.to) {
							moves = append(moves, move)
						}
					}
				}
			} else if charBelow == '#' {
				// we are on row 1 so make sure that the place we're stepping to isn't hovering
				// over an endzone.
				moves = append(moves, Move{to: point, kind: a.kind, energy: int(a.kind) * (depth + 1)})
			}

			oldPos := a.pos
			oldChar := c.board[oldPos.y][oldPos.x]

			a.pos = point
			c.board[oldPos.y][oldPos.x] = '.'
			c.board[point.y][point.x] = oldChar

			// search
			deeperMoves := c.validMovesInner(a, depth+1, seen)
			moves = append(moves, deeperMoves...)

			// unmake the move
			a.pos = oldPos
			c.board[oldPos.y][oldPos.x] = oldChar
			c.board[point.y][point.x] = '.'
		}
	}

	// sort moves by those that could be winners first
	sort.Slice(moves, func(i, j int) bool {
		return moves[i].to.y > moves[j].to.y
	})
	return moves
}

func (c *Cave) validMoves(a *Amphipod) []Move {
	return c.validMovesInner(a, 0, map[Point]bool{})
}

func (c *Cave) hash() string {
	h := sha256.New()
	for y := 0; y < len(c.board); y++ {
		h.Write([]byte(string(c.board[y])))
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}

func (m Move) String() string {
	str := "A"
	switch m.kind {
	case Bronze:
		str = "B"
	case Copper:
		str = "C"
	case Desert:
		str = "D"
	}
	return fmt.Sprintf("%s to:%v", str, m.to)
}

func solveAmphi(c *Cave, energy int, minEnergy int, cache map[string]int, depth int, debug bool, count *int) int {
	(*count)++
	if *count%100000 == 0 {
		fmt.Println("searched", *count, "moves")
		fmt.Println("\nDepth:", depth)
		fmt.Println(c)
		fmt.Println("")
	}

	hash := c.hash()

	if e, found := cache[hash]; found {
		// fmt.Println("cache hit")
		return e
	}

	if c.isOrganized() {
		fmt.Println("cave is organized at depth:", depth, "energy:", energy)
		// don't cache a win!  we could find a cheaper one.
		return energy
	}

	hadMoves := false

	for _, a := range c.amphipods {
		if c.amphiIsHome(a) {
			continue
		}

		moves := c.validMoves(a)
		for _, move := range moves {
			// can't move because a is locked in place and this move wouldn't take it home.
			if a.pos.y == 1 && !c.amphiWouldBeHome(a, move.to) {
				continue
			}

			hadMoves = true

			// make the move
			oldPos := a.pos
			oldChar := c.board[oldPos.y][oldPos.x]

			a.pos = move.to
			c.board[oldPos.y][oldPos.x] = '.'
			c.board[move.to.y][move.to.x] = oldChar

			if debug {
				fmt.Println("\nDepth:", depth)
				fmt.Println(c)
				// fmt.Scanln()
			}

			// search
			subtreeEnergy := solveAmphi(c, energy+move.energy, minEnergy, cache, depth+1, debug, count)
			if subtreeEnergy < minEnergy {
				minEnergy = subtreeEnergy
			}

			// unmake the move
			a.pos = oldPos
			c.board[oldPos.y][oldPos.x] = oldChar
			c.board[move.to.y][move.to.x] = '.'
		}
	}

	if !hadMoves {
		// fmt.Println("out of moves")
		// no moves to make and we aren't organized... this is a dead end
		cache[hash] = math.MaxInt32
		return math.MaxInt32
	}

	cache[hash] = minEnergy

	return minEnergy
}

func Day23_1() {
	// f, err := os.Create("/tmp/cpu.pprof")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// pprof.StartCPUProfile(f)
	// defer pprof.StopCPUProfile()

	// cave := loadAmphipodCave(false)
	// i := 0
	// energy := solveAmphi(&cave, 0, math.MaxInt32, map[string]int{}, 0, false, &i)
	// fmt.Println("day 23-1:", energy, "(energy 2 deep)")
}

func Day23_2() {
	cave := loadAmphipodCave(true)
	fmt.Println(&cave)
	i := 0
	energy := solveAmphi(&cave, 0, math.MaxInt32, map[string]int{}, 0, false, &i)
	fmt.Println("day 23-2:", energy, "(energy 4 deep)")
}
