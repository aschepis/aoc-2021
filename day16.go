package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

var hexToBinary map[rune]string = map[rune]string{
	'0': "0000",
	'1': "0001",
	'2': "0010",
	'3': "0011",
	'4': "0100",
	'5': "0101",
	'6': "0110",
	'7': "0111",
	'8': "1000",
	'9': "1001",
	'A': "1010",
	'B': "1011",
	'C': "1100",
	'D': "1101",
	'E': "1110",
	'F': "1111",
}

var optype map[uint64]string = map[uint64]string{
	0: "sum",
	1: "product",
	2: "min",
	3: "max",
	4: "value",
	5: "greater",
	6: "less",
	7: "equal",
}

type BITSPacketTreeNode struct {
	packet     *BITSPacket
	subpackets []*BITSPacketTreeNode
}

func (t *BITSPacketTreeNode) versionSum() uint64 {
	sum := t.packet.version()
	fmt.Println("packet version:", t.packet.version(), "rep:", t.packet.binaryRep)
	for _, p := range t.subpackets {
		sum += p.versionSum()
	}
	return sum
}

func (t *BITSPacketTreeNode) printNode(parent int, i *int, f *os.File) {
	(*i)++
	nodeId := (*i) + 1
	if parent > 0 {
		fmt.Fprintf(f, "  n%d -> n%d\n", parent, nodeId)
	}

	if optype[t.packet.packetType()] == "value" {
		fmt.Fprintf(f, "  n%d [label=\"%d\"]\n", nodeId, t.packet.value())
	} else {
		fmt.Fprintf(f, "  n%d [label=\"%s\"]\n", nodeId, optype[t.packet.packetType()])
	}
	for _, p := range t.subpackets {
		p.printNode(nodeId, i, f)
	}
}

func printDigraph(head *BITSPacketTreeNode, filename string) {
	f, _ := os.Create(filename)
	defer f.Close()
	fmt.Fprintf(f, "digraph \"operations\" {\n")
	id := 0
	head.printNode(0, &id, f)
	fmt.Fprintf(f, "}\n")
}

func (t *BITSPacketTreeNode) value() uint64 {
	packetType := t.packet.packetType()
	switch packetType {
	case 0:
		// fmt.Printf("(%d)+(%d)\n", t.left.value(), t.right.value())
		// sum
		sum := uint64(0)
		for _, p := range t.subpackets {
			sum += p.value()
		}
		return sum
	case 1:
		// fmt.Printf("(%d)*(%d)\n", t.left.value(), t.right.value())
		// product
		product := uint64(1)
		for _, p := range t.subpackets {
			product = product * p.value()
		}

		return product
	case 2:
		// min
		// fmt.Printf("min(%d,%d)\n", t.left.value(), t.right.value())
		min := uint64(math.MaxUint64)
		for _, p := range t.subpackets {
			if p.value() < min {
				min = p.value()
			}
		}
		return min
	case 3:
		// max
		// fmt.Printf("max(%d,%d)\n", t.left.value(), t.right.value())
		max := uint64(0)
		for _, p := range t.subpackets {
			if p.value() > max {
				max = p.value()
			}
		}
		return max
	case 4:
		return t.packet.value()
	case 5:
		// greater
		// fmt.Printf("(%d) > (%d)\n", t.left.value(), t.right.value())
		l := t.subpackets[0].value()
		r := t.subpackets[1].value()
		if l > r {
			return 1
		} else {
			return 0
		}
	case 6:
		// less
		// fmt.Printf("(%d) < (%d)\n", t.left.value(), t.right.value())
		l := t.subpackets[0].value()
		r := t.subpackets[1].value()
		if l < r {
			return 1
		} else {
			return 0
		}
	case 7:
		// equal
		// fmt.Printf("(%d) == (%d)\n", t.left.value(), t.right.value())
		l := t.subpackets[0].value()
		r := t.subpackets[1].value()
		if l == r {
			return 1
		} else {
			return 0
		}
	}
	panic(fmt.Sprintf("you shouldn't be here: %d", packetType))
}

type BITSPacket struct {
	binaryRep string
}

func (p *BITSPacket) version() uint64 {
	return hexStrToUint64(p.binaryRep[0:3])
}

func (p *BITSPacket) value() uint64 {
	valueSection := p.binaryRep[6:]
	binary := ""
	for len(valueSection) > 0 {
		binary = fmt.Sprintf("%s%s", binary, valueSection[1:5])
		valueSection = valueSection[5:]
	}

	return hexStrToUint64(binary)
}

func (p *BITSPacket) packetType() uint64 {
	return hexStrToUint64(p.binaryRep[3:6])
}

type BITSMessage struct {
	binaryRep string
}

func (m *BITSMessage) parsePacket(binaryMessage string, maxRead, numPackets int, node *BITSPacketTreeNode) (*BITSPacketTreeNode, int) {
	cursor := 0
	var head *BITSPacketTreeNode = nil
	var curr *BITSPacketTreeNode = nil
	packetCount := 0
	for {
		if maxRead > 0 && cursor >= maxRead {
			break
		}
		if numPackets != 0 && packetCount >= numPackets {
			break
		}
		if hexStrToUint64(binaryMessage[cursor:]) == 0 {
			// no more packets
			break
		}

		packet := &BITSPacket{}
		// version := hexStrToUint64(binaryMessage[cursor : cursor+3])
		// fmt.Println("version:", version)
		pType := hexStrToUint64(binaryMessage[cursor+3 : cursor+6])
		packet.binaryRep = binaryMessage[cursor : cursor+6]
		cursor += 6
		if pType == 4 {
			// literal value. parse messages until the last
			for binaryMessage[cursor] != '0' {
				packet.binaryRep = fmt.Sprintf("%s%s", packet.binaryRep, binaryMessage[cursor:cursor+5])
				cursor += 5
			}

			// last value
			packet.binaryRep = fmt.Sprintf("%s%s", packet.binaryRep, binaryMessage[cursor:cursor+5])
			cursor += 5

			treeNode := &BITSPacketTreeNode{packet: packet}
			if head == nil {
				head = treeNode
			} else {
				curr.subpackets = append(curr.subpackets, treeNode)
			}
			curr = treeNode
			packetCount++
		} else {
			// operator
			lengthTypeId := hexStrToUint64(string(binaryMessage[cursor]))
			packet.binaryRep = fmt.Sprintf("%s%s", packet.binaryRep, string(binaryMessage[cursor]))
			cursor++

			treeNode := &BITSPacketTreeNode{packet: packet}
			if head == nil {
				head = treeNode
			} else {
				curr.subpackets = append(curr.subpackets, treeNode)
			}
			curr = treeNode
			var node *BITSPacketTreeNode = curr
			if lengthTypeId == 0 {
				// total length of bits
				totalLength := hexStrToUint64(binaryMessage[cursor : cursor+15])
				// fmt.Println("operator", optype[pType], "- bits:", totalLength)

				packet.binaryRep = fmt.Sprintf("%s%s", packet.binaryRep, binaryMessage[cursor:cursor+15])
				cursor += 15

				totalInc := 0
				i := 0
				for totalInc < int(totalLength) {
					next, inc := m.parsePacket(binaryMessage[cursor+totalInc:], 0, 1, curr)
					totalInc += inc
					curr.subpackets = append(node.subpackets, next)
					i++
				}
				cursor += totalInc
			} else if lengthTypeId == 1 {
				// total # of sub packets
				numPackets := hexStrToUint64(binaryMessage[cursor : cursor+11])
				// fmt.Println("operator", optype[pType], "- packets:", numPackets)

				packet.binaryRep = fmt.Sprintf("%s%s", packet.binaryRep, binaryMessage[cursor:cursor+11])
				cursor += 11

				for i := 0; i < int(numPackets); i++ {
					next, cursorInc := m.parsePacket(binaryMessage[cursor:], 0, 1, curr)
					cursor += cursorInc
					curr.subpackets = append(node.subpackets, next)
				}
			} else {
				panic(fmt.Sprintf("bad length type id: %d", lengthTypeId))
			}
			packetCount++
		}
	}
	return head, cursor
}

func (m *BITSMessage) packetTree() *BITSPacketTreeNode {
	head, _ := m.parsePacket(m.binaryRep, len(m.binaryRep), 0, nil)
	return head
}

func loadBITSMessage() BITSMessage {
	file, _ := os.Open("day16.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	message := BITSMessage{}
	for scanner.Scan() {
		line := scanner.Text()
		for _, b := range line {
			message.binaryRep = fmt.Sprintf("%s%s", message.binaryRep, hexToBinary[b])
		}
	}

	return message
}

func Day16_1() {
	message := loadBITSMessage()
	tree := message.packetTree()
	fmt.Println("day 16-1:", tree.versionSum(), "(version sum)")
}

func Day16_2() {
	message := loadBITSMessage()
	tree := message.packetTree()
	printDigraph(tree, "/tmp/graph.dot")
	// 3408662834145
	fmt.Println("day 16-2:", tree.value(), "(tree sum)")
}
