package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"regexp"
)

type Point struct {
	x, y int
}

type Point3D struct {
	x, y, z int
}

type CoordinateRange3D struct {
	x, y, z IntRange
}

func (r CoordinateRange3D) overlaps(other CoordinateRange3D) bool {
	overlapX := r.x.overlaps(other.x)
	overlapY := r.y.overlaps(other.y)
	overlapZ := r.z.overlaps(other.z)
	return overlapX && overlapY && overlapZ
}

type RuneStack struct {
	values []rune
}

func (s *RuneStack) Push(val rune) {
	s.values = append(s.values, val)
}

func (s *RuneStack) Pop() (rune, error) {
	if len(s.values) == 0 {
		return rune(0), fmt.Errorf("stack is empty")
	}
	last := s.values[len(s.values)-1]
	s.values = s.values[0 : len(s.values)-1]
	return last, nil
}

func (s *RuneStack) Len() int {
	return len(s.values)
}

func copyMap(in, out interface{}) {
	buf := new(bytes.Buffer)
	gob.NewEncoder(buf).Encode(in)
	gob.NewDecoder(buf).Decode(out)
}

func reverseString(s string) string {
	a := []byte(s)
	for i, j := 0, len(s)-1; i < j; i++ {
		a[i], a[j] = a[j], a[i]
		j--
	}
	return string(a)
}

func hexStrToUint64(str string) uint64 {
	out := uint64(0)
	for i, b := range reverseString(str) {
		switch b {
		case '1':
			out += 1 << i
		}
	}
	return out
}

// pass in a regex and string w/ group names
func matchStringMap(r *regexp.Regexp, str string) map[string]string {
	matchMap := map[string]string{}
	matches := r.FindStringSubmatch(str)
	for i, groupName := range r.SubexpNames() {
		matchMap[groupName] = matches[i]
	}
	return matchMap
}

type IntRange struct {
	min, max int
}

func (r IntRange) overlaps(other IntRange) bool {
	tooLow := other.max < r.min
	tooHigh := other.min > r.max
	return !(tooLow || tooHigh)
}

type CoordinateRange struct {
	x, y IntRange
}
