package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type instruction struct {
	dir       string
	magnitude int
}

func day2Instructions() []instruction {
	instructions := []instruction{}

	file, err := os.Open("day2.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, " ")
		val, err := strconv.Atoi(parts[1])
		if err != nil {
			log.Fatal(err)
		}

		instructions = append(instructions, instruction{dir: parts[0], magnitude: val})
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return instructions
}

func Day2_1() {
	x := 0
	depth := 0

	for _, instruction := range day2Instructions() {
		switch instruction.dir {
		case "forward":
			x += instruction.magnitude
		case "up":
			depth -= instruction.magnitude
		case "down":
			depth += instruction.magnitude
		}
	}

	fmt.Println("day 2-1:", x*depth, "(depth product)")
}

func Day2_2() {
	x := 0
	depth := 0
	aim := 0

	for _, instruction := range day2Instructions() {
		switch instruction.dir {
		case "forward":
			x += instruction.magnitude
			depth += aim * instruction.magnitude
		case "up":
			aim -= instruction.magnitude
		case "down":
			aim += instruction.magnitude
		}
	}

	fmt.Println("day 2-1:", x*depth, "(depth product with aim)")
}
